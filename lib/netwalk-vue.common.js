module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "fb15");
/******/ })
/************************************************************************/
/******/ ({

/***/ "00fd":
/***/ (function(module, exports, __webpack_require__) {

var Symbol = __webpack_require__("9e69");

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString = objectProto.toString;

/** Built-in value references. */
var symToStringTag = Symbol ? Symbol.toStringTag : undefined;

/**
 * A specialized version of `baseGetTag` which ignores `Symbol.toStringTag` values.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the raw `toStringTag`.
 */
function getRawTag(value) {
  var isOwn = hasOwnProperty.call(value, symToStringTag),
      tag = value[symToStringTag];

  try {
    value[symToStringTag] = undefined;
    var unmasked = true;
  } catch (e) {}

  var result = nativeObjectToString.call(value);
  if (unmasked) {
    if (isOwn) {
      value[symToStringTag] = tag;
    } else {
      delete value[symToStringTag];
    }
  }
  return result;
}

module.exports = getRawTag;


/***/ }),

/***/ "09f2":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_0_0_node_modules_css_loader_index_js_ref_8_oneOf_0_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_0_2_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_0_3_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_0_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NetwalkGame_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("c6ff");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_0_0_node_modules_css_loader_index_js_ref_8_oneOf_0_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_0_2_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_0_3_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_0_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NetwalkGame_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_0_0_node_modules_css_loader_index_js_ref_8_oneOf_0_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_0_2_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_0_3_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_0_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NetwalkGame_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0__);
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_0_0_node_modules_css_loader_index_js_ref_8_oneOf_0_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_0_2_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_0_3_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_0_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NetwalkGame_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "1310":
/***/ (function(module, exports) {

/**
 * Checks if `value` is object-like. A value is object-like if it's not `null`
 * and has a `typeof` result of "object".
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 * @example
 *
 * _.isObjectLike({});
 * // => true
 *
 * _.isObjectLike([1, 2, 3]);
 * // => true
 *
 * _.isObjectLike(_.noop);
 * // => false
 *
 * _.isObjectLike(null);
 * // => false
 */
function isObjectLike(value) {
  return value != null && typeof value == 'object';
}

module.exports = isObjectLike;


/***/ }),

/***/ "1743":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _elementResizeDetector = _interopRequireDefault(__webpack_require__("eec4"));

var _debounce2 = _interopRequireDefault(__webpack_require__("b047"));

var _PlayingField = _interopRequireDefault(__webpack_require__("febb"));

var _netwalkCore = __webpack_require__("92e8");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var erd = (0, _elementResizeDetector.default)();
var baseStyleVars = {
  cellSizePx: 50,
  connectionWidthPercent: 20,
  connectionColor: '#e1e1e1',
  connectionColorConnected: '#6b8fd4',
  cellColor: 'transparent',
  emptyCellColor: 'transparent',
  providerColor: '#ff6200',
  consumerColor: '#ffdc73',
  consumerColorConnected: '#ffbf00',
  rotateAnimation: true,
  rotateAnimationDuration: '200ms',
  colorAnimation: true,
  colorAnimationDuration: '150ms'
};
var _default = {
  name: 'NetwalkGame',
  components: {
    'playing-field': _PlayingField.default
  },
  props: {
    fill: {
      type: Boolean,
      default: true
    },
    fillAnimationDelay: {
      type: Number,
      default: 0
    },
    randomize: {
      type: Boolean,
      default: true
    },
    randomizeAnimationDelay: {
      type: Number,
      default: 0
    },
    template: {
      type: Array
    },
    customStyle: {
      type: Object
    }
  },
  data: function data() {
    return {
      filling: false,
      filled: false,
      randomizing: false,
      randomized: false,
      ready: false,
      started: false,
      finished: false,
      moves: 0,
      stopwatch: 0,
      matrix: [],
      matrixDimensions: {
        rows: 0,
        columns: 0
      },
      erd: false
    };
  },
  computed: {
    styleVars: function styleVars() {
      return Object.assign(baseStyleVars, this.customStyle);
    }
  },
  mounted: function mounted() {
    if (this.template) {
      this.createNetwalkGame({
        template: this.template
      });
    } else {
      this.installPlayingFiledWrapperWatcher();
    }
  },
  watch: {
    matrixDimensions: {
      handler: function handler(dimensions) {
        this.createNetwalkGame(dimensions);
      },
      deep: true
    },
    template: {
      handler: function handler(template) {
        if (template) {
          if (this.erd) this.uninstallPlayingFiledWrapperWatcher();
          this.createNetwalkGame({
            template: template
          });
        } else {
          this.installPlayingFiledWrapperWatcher();
        }
      }
    },
    filling: {
      immediate: true,
      handler: function handler(value) {
        this.$emit('filling-change', value);
      }
    },
    filled: {
      immediate: true,
      handler: function handler(value) {
        this.$emit('filled-change', value);
      }
    },
    randomizing: {
      immediate: true,
      handler: function handler(value) {
        this.$emit('randomizing-change', value);
      }
    },
    randomized: {
      immediate: true,
      handler: function handler(value) {
        this.$emit('randomized-change', value);
      }
    },
    moves: {
      immediate: true,
      handler: function handler(value) {
        this.$emit('moves-change', value);
      }
    },
    stopwatch: {
      immediate: true,
      handler: function handler(value) {
        this.$emit('stopwatch-change', value);
      }
    },
    finished: {
      immediate: true,
      handler: function handler(value) {
        this.$emit('finished-change', value);
      }
    },
    started: {
      immediate: true,
      handler: function handler(value) {
        this.$emit('started-change', value);
      }
    }
  },
  methods: {
    playAgain: function playAgain() {
      this.createNetwalkGame();
    },
    getMatrixDimensions: function getMatrixDimensions(width, height, cellSize) {
      function floorBy(value, multiplier) {
        return value >= multiplier ? Math.floor(value / multiplier) * multiplier : multiplier;
      }

      var flooredWidth = floorBy(width, cellSize);
      var flooredHeight = floorBy(height, cellSize);
      var columns = flooredWidth / cellSize;
      var rows = flooredHeight / cellSize;
      return {
        rows: rows,
        columns: columns
      };
    },
    installPlayingFiledWrapperWatcher: function installPlayingFiledWrapperWatcher() {
      var _this = this;

      this.erd = true;
      erd.listenTo(this.$refs.playingFieldWrapper, (0, _debounce2.default)(function () {
        var playingFieldMaxW = _this.$refs.playingFieldWrapper.offsetWidth;
        var playingFieldMaxH = _this.$refs.playingFieldWrapper.offsetHeight;
        Object.assign(_this.matrixDimensions, _this.getMatrixDimensions(playingFieldMaxW, playingFieldMaxH, _this.styleVars.cellSizePx));
      }, 300));
      this.$once('hook:beforeDestroy', function () {
        if (erd) erd.uninstall(this.$refs.playingFieldWrapper);
      });
    },
    uninstallPlayingFiledWrapperWatcher: function uninstallPlayingFiledWrapperWatcher() {
      this.erd = false;
      erd.uninstall(this.$refs.playingFieldWrapper);
    },
    createNetwalkGame: function createNetwalkGame() {
      var _this2 = this;

      if (this.netwalk) this.netwalk.destroy();
      var gameOptions = {
        fill: this.fill,
        fillAnimationDelay: this.fillAnimationDelay,
        randomize: this.randomize,
        randomizeAnimationDelay: this.randomizeAnimationDelay,
        movesChangeCallback: function movesChangeCallback(value) {
          _this2.moves = value;
        },
        stopwatchChangeCallback: function stopwatchChangeCallback(value) {
          _this2.stopwatch = value;
        },
        finishedChangeCallback: function finishedChangeCallback(value) {
          _this2.finished = value;
        },
        startedChangeCallback: function startedChangeCallback(value) {
          _this2.started = value;
        },
        matrixFillingChangeCallback: function matrixFillingChangeCallback(value) {
          _this2.filling = value;
        },
        matrixFilledChangeCallback: function matrixFilledChangeCallback(value) {
          _this2.filled = value;
        },
        matrixRandomizingChangeCallback: function matrixRandomizingChangeCallback(value) {
          _this2.randomizing = value;
        },
        matrixRandomizedChangeCallback: function matrixRandomizedChangeCallback(value) {
          _this2.randomized = value;
        } // matrixChangeCallback: () => { console.log(`matrix change`) }

      };

      if (this.template) {
        Object.assign(gameOptions, {
          template: this.template
        });
      } else {
        Object.assign(gameOptions, _objectSpread({}, this.matrixDimensions));
      }

      this.netwalk = new _netwalkCore.Netwalk(gameOptions);
      this.matrix = this.netwalk.matrix;
    },
    rotateNode: function rotateNode(id) {
      this.netwalk.rotateNode(id);
    }
  }
};
exports.default = _default;

/***/ }),

/***/ "18d2":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Resize detection strategy that injects objects to elements in order to detect resize events.
 * Heavily inspired by: http://www.backalleycoder.com/2013/03/18/cross-browser-event-based-element-resize-detection/
 */



var browserDetector = __webpack_require__("18e9");

module.exports = function(options) {
    options             = options || {};
    var reporter        = options.reporter;
    var batchProcessor  = options.batchProcessor;
    var getState        = options.stateHandler.getState;

    if(!reporter) {
        throw new Error("Missing required dependency: reporter.");
    }

    /**
     * Adds a resize event listener to the element.
     * @public
     * @param {element} element The element that should have the listener added.
     * @param {function} listener The listener callback to be called for each resize event of the element. The element will be given as a parameter to the listener callback.
     */
    function addListener(element, listener) {
        if(!getObject(element)) {
            throw new Error("Element is not detectable by this strategy.");
        }

        function listenerProxy() {
            listener(element);
        }

        if(browserDetector.isIE(8)) {
            //IE 8 does not support object, but supports the resize event directly on elements.
            getState(element).object = {
                proxy: listenerProxy
            };
            element.attachEvent("onresize", listenerProxy);
        } else {
            var object = getObject(element);
            object.contentDocument.defaultView.addEventListener("resize", listenerProxy);
        }
    }

    /**
     * Makes an element detectable and ready to be listened for resize events. Will call the callback when the element is ready to be listened for resize changes.
     * @private
     * @param {object} options Optional options object.
     * @param {element} element The element to make detectable
     * @param {function} callback The callback to be called when the element is ready to be listened for resize changes. Will be called with the element as first parameter.
     */
    function makeDetectable(options, element, callback) {
        if (!callback) {
            callback = element;
            element = options;
            options = null;
        }

        options = options || {};
        var debug = options.debug;

        function injectObject(element, callback) {
            var OBJECT_STYLE = "display: block; position: absolute; top: 0; left: 0; width: 100%; height: 100%; border: none; padding: 0; margin: 0; opacity: 0; z-index: -1000; pointer-events: none;";

            //The target element needs to be positioned (everything except static) so the absolute positioned object will be positioned relative to the target element.

            // Position altering may be performed directly or on object load, depending on if style resolution is possible directly or not.
            var positionCheckPerformed = false;

            // The element may not yet be attached to the DOM, and therefore the style object may be empty in some browsers.
            // Since the style object is a reference, it will be updated as soon as the element is attached to the DOM.
            var style = window.getComputedStyle(element);
            var width = element.offsetWidth;
            var height = element.offsetHeight;

            getState(element).startSize = {
                width: width,
                height: height
            };

            function mutateDom() {
                function alterPositionStyles() {
                    if(style.position === "static") {
                        element.style.position = "relative";

                        var removeRelativeStyles = function(reporter, element, style, property) {
                            function getNumericalValue(value) {
                                return value.replace(/[^-\d\.]/g, "");
                            }

                            var value = style[property];

                            if(value !== "auto" && getNumericalValue(value) !== "0") {
                                reporter.warn("An element that is positioned static has style." + property + "=" + value + " which is ignored due to the static positioning. The element will need to be positioned relative, so the style." + property + " will be set to 0. Element: ", element);
                                element.style[property] = 0;
                            }
                        };

                        //Check so that there are no accidental styles that will make the element styled differently now that is is relative.
                        //If there are any, set them to 0 (this should be okay with the user since the style properties did nothing before [since the element was positioned static] anyway).
                        removeRelativeStyles(reporter, element, style, "top");
                        removeRelativeStyles(reporter, element, style, "right");
                        removeRelativeStyles(reporter, element, style, "bottom");
                        removeRelativeStyles(reporter, element, style, "left");
                    }
                }

                function onObjectLoad() {
                    // The object has been loaded, which means that the element now is guaranteed to be attached to the DOM.
                    if (!positionCheckPerformed) {
                        alterPositionStyles();
                    }

                    /*jshint validthis: true */

                    function getDocument(element, callback) {
                        //Opera 12 seem to call the object.onload before the actual document has been created.
                        //So if it is not present, poll it with an timeout until it is present.
                        //TODO: Could maybe be handled better with object.onreadystatechange or similar.
                        if(!element.contentDocument) {
                            setTimeout(function checkForObjectDocument() {
                                getDocument(element, callback);
                            }, 100);

                            return;
                        }

                        callback(element.contentDocument);
                    }

                    //Mutating the object element here seems to fire another load event.
                    //Mutating the inner document of the object element is fine though.
                    var objectElement = this;

                    //Create the style element to be added to the object.
                    getDocument(objectElement, function onObjectDocumentReady(objectDocument) {
                        //Notify that the element is ready to be listened to.
                        callback(element);
                    });
                }

                // The element may be detached from the DOM, and some browsers does not support style resolving of detached elements.
                // The alterPositionStyles needs to be delayed until we know the element has been attached to the DOM (which we are sure of when the onObjectLoad has been fired), if style resolution is not possible.
                if (style.position !== "") {
                    alterPositionStyles(style);
                    positionCheckPerformed = true;
                }

                //Add an object element as a child to the target element that will be listened to for resize events.
                var object = document.createElement("object");
                object.style.cssText = OBJECT_STYLE;
                object.tabIndex = -1;
                object.type = "text/html";
                object.onload = onObjectLoad;

                //Safari: This must occur before adding the object to the DOM.
                //IE: Does not like that this happens before, even if it is also added after.
                if(!browserDetector.isIE()) {
                    object.data = "about:blank";
                }

                element.appendChild(object);
                getState(element).object = object;

                //IE: This must occur after adding the object to the DOM.
                if(browserDetector.isIE()) {
                    object.data = "about:blank";
                }
            }

            if(batchProcessor) {
                batchProcessor.add(mutateDom);
            } else {
                mutateDom();
            }
        }

        if(browserDetector.isIE(8)) {
            //IE 8 does not support objects properly. Luckily they do support the resize event.
            //So do not inject the object and notify that the element is already ready to be listened to.
            //The event handler for the resize event is attached in the utils.addListener instead.
            callback(element);
        } else {
            injectObject(element, callback);
        }
    }

    /**
     * Returns the child object of the target element.
     * @private
     * @param {element} element The target element.
     * @returns The object element of the target.
     */
    function getObject(element) {
        return getState(element).object;
    }

    function uninstall(element) {
        if(browserDetector.isIE(8)) {
            element.detachEvent("onresize", getState(element).object.proxy);
        } else {
            element.removeChild(getObject(element));
        }
        delete getState(element).object;
    }

    return {
        makeDetectable: makeDetectable,
        addListener: addListener,
        uninstall: uninstall
    };
};


/***/ }),

/***/ "18e9":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var detector = module.exports = {};

detector.isIE = function(version) {
    function isAnyIeVersion() {
        var agent = navigator.userAgent.toLowerCase();
        return agent.indexOf("msie") !== -1 || agent.indexOf("trident") !== -1 || agent.indexOf(" edge/") !== -1;
    }

    if(!isAnyIeVersion()) {
        return false;
    }

    if(!version) {
        return true;
    }

    //Shamelessly stolen from https://gist.github.com/padolsey/527683
    var ieVersion = (function(){
        var undef,
            v = 3,
            div = document.createElement("div"),
            all = div.getElementsByTagName("i");

        do {
            div.innerHTML = "<!--[if gt IE " + (++v) + "]><i></i><![endif]-->";
        }
        while (all[0]);

        return v > 4 ? v : undef;
    }());

    return version === ieVersion;
};

detector.isLegacyOpera = function() {
    return !!window.opera;
};


/***/ }),

/***/ "1a8c":
/***/ (function(module, exports) {

/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = typeof value;
  return value != null && (type == 'object' || type == 'function');
}

module.exports = isObject;


/***/ }),

/***/ "1c73":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_0_0_node_modules_css_loader_index_js_ref_8_oneOf_0_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_0_2_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_0_3_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_0_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayingField_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("4ec5");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_0_0_node_modules_css_loader_index_js_ref_8_oneOf_0_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_0_2_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_0_3_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_0_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayingField_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_0_0_node_modules_css_loader_index_js_ref_8_oneOf_0_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_0_2_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_0_3_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_0_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayingField_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0__);
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_0_0_node_modules_css_loader_index_js_ref_8_oneOf_0_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_0_2_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_0_3_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_0_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayingField_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "1df4":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "\n.NetwalkGame_container_17-cs{-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;position:relative\n}\n.NetwalkGame_container_17-cs,.NetwalkGame_playing-field-wrapper_2zwYS{height:100%;width:100%;display:-webkit-box;display:-ms-flexbox;display:flex\n}\n.NetwalkGame_playing-field-wrapper_2zwYS{-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;overflow:hidden\n}", ""]);

// exports
exports.locals = {
	"container": "NetwalkGame_container_17-cs",
	"playing-field-wrapper": "NetwalkGame_playing-field-wrapper_2zwYS"
};

/***/ }),

/***/ "1eb2":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// This file is imported into lib/wc client bundles.

if (typeof window !== 'undefined') {
  var i
  if ((i = window.document.currentScript) && (i = i.src.match(/(.+\/)[^/]+\.js$/))) {
    __webpack_require__.p = i[1] // eslint-disable-line
  }
}

// Indicate to webpack that this file can be concatenated
/* unused harmony default export */ var _unused_webpack_default_export = (null);


/***/ }),

/***/ "2350":
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),

/***/ "26fd":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_0_0_node_modules_css_loader_index_js_ref_8_oneOf_0_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_0_2_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_0_3_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_0_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Cell_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("91ce");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_0_0_node_modules_css_loader_index_js_ref_8_oneOf_0_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_0_2_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_0_3_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_0_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Cell_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_0_0_node_modules_css_loader_index_js_ref_8_oneOf_0_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_0_2_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_0_3_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_0_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Cell_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0__);
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_0_0_node_modules_css_loader_index_js_ref_8_oneOf_0_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_0_2_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_0_3_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_0_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Cell_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "2877":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "29f3":
/***/ (function(module, exports) {

/** Used for built-in method references. */
var objectProto = Object.prototype;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString = objectProto.toString;

/**
 * Converts `value` to a string using `Object.prototype.toString`.
 *
 * @private
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 */
function objectToString(value) {
  return nativeObjectToString.call(value);
}

module.exports = objectToString;


/***/ }),

/***/ "2b3e":
/***/ (function(module, exports, __webpack_require__) {

var freeGlobal = __webpack_require__("585a");

/** Detect free variable `self`. */
var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

/** Used as a reference to the global object. */
var root = freeGlobal || freeSelf || Function('return this')();

module.exports = root;


/***/ }),

/***/ "2cef":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function() {
    var idCount = 1;

    /**
     * Generates a new unique id in the context.
     * @public
     * @returns {number} A unique id in the context.
     */
    function generate() {
        return idCount++;
    }

    return {
        generate: generate
    };
};


/***/ }),

/***/ "2d07":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _netwalkCore = __webpack_require__("92e8");

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
var _default = {
  name: 'Cell',
  components: {},
  data: function data() {
    return {
      fullCircleCounter: 0
    };
  },
  props: {
    type: {
      validator: function validator(value) {
        return value === null || typeof value === 'string';
      },
      required: true
    },
    connected: {
      type: Boolean,
      required: true
    },
    connections: {
      type: Array,
      required: true
    },
    styleVars: {
      type: Object,
      required: true
    }
  },
  watch: {
    connectionParams: function connectionParams(newParams, prevParams) {
      if (newParams !== null && prevParams !== null) {
        if (prevParams.position !== 0 && newParams.position === 0) {
          this.fullCircleCounter++;
        }
      }
    }
  },
  computed: {
    connectionParams: function connectionParams() {
      return _netwalkCore.Cell.getConnectionParamsByConnections(this.connections);
    },
    connectionsTransition: function connectionsTransition() {
      var _this$styleVars = this.styleVars,
          rotateAnimation = _this$styleVars.rotateAnimation,
          rotateAnimationDuration = _this$styleVars.rotateAnimationDuration,
          colorAnimation = _this$styleVars.colorAnimation,
          colorAnimationDuration = _this$styleVars.colorAnimationDuration;
      var transformProperty = rotateAnimation ? "transform ".concat(rotateAnimationDuration) : '';
      var fillProperty = colorAnimation ? "fill ".concat(colorAnimationDuration) : '';
      var fillDelay = transformProperty && fillProperty && this.connected ? rotateAnimationDuration : '';
      var fullFillProperty = "".concat(fillProperty, " ").concat(fillDelay);
      return transformProperty + (transformProperty && fullFillProperty ? ', ' : '') + fullFillProperty;
    },
    consumerTransition: function consumerTransition() {
      var _this$styleVars2 = this.styleVars,
          rotateAnimation = _this$styleVars2.rotateAnimation,
          rotateAnimationDuration = _this$styleVars2.rotateAnimationDuration,
          colorAnimation = _this$styleVars2.colorAnimation,
          colorAnimationDuration = _this$styleVars2.colorAnimationDuration;
      var property = colorAnimation ? 'background-color' : '';
      var duration = colorAnimation ? colorAnimationDuration : '';
      var delay = rotateAnimation && this.connected ? rotateAnimationDuration : '';
      return "".concat(property, " ").concat(duration, " ").concat(delay);
    },
    rotationAngle: function rotationAngle() {
      var _this$connectionParam = this.connectionParams,
          position = _this$connectionParam.position,
          type = _this$connectionParam.type;
      return position * 90 + this.fullCircleCounter * (_netwalkCore.connectionTypes[type].length * 90);
    }
  }
};
exports.default = _default;

/***/ }),

/***/ "323f":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"2867f158-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/NetwalkGame.vue?vue&type=template&id=769a2275&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{class:_vm.$style.container},[_c('div',{ref:"playingFieldWrapper",class:_vm.$style['playing-field-wrapper']},[_c('playing-field',{attrs:{"matrix":_vm.matrix,"style-vars":_vm.styleVars},on:{"rotate-node":_vm.rotateNode}})],1)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./src/components/NetwalkGame.vue?vue&type=template&id=769a2275&
/* concated harmony reexport render */__webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* concated harmony reexport staticRenderFns */__webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });


/***/ }),

/***/ "3729":
/***/ (function(module, exports, __webpack_require__) {

var Symbol = __webpack_require__("9e69"),
    getRawTag = __webpack_require__("00fd"),
    objectToString = __webpack_require__("29f3");

/** `Object#toString` result references. */
var nullTag = '[object Null]',
    undefinedTag = '[object Undefined]';

/** Built-in value references. */
var symToStringTag = Symbol ? Symbol.toStringTag : undefined;

/**
 * The base implementation of `getTag` without fallbacks for buggy environments.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */
function baseGetTag(value) {
  if (value == null) {
    return value === undefined ? undefinedTag : nullTag;
  }
  return (symToStringTag && symToStringTag in Object(value))
    ? getRawTag(value)
    : objectToString(value);
}

module.exports = baseGetTag;


/***/ }),

/***/ "408c":
/***/ (function(module, exports, __webpack_require__) {

var root = __webpack_require__("2b3e");

/**
 * Gets the timestamp of the number of milliseconds that have elapsed since
 * the Unix epoch (1 January 1970 00:00:00 UTC).
 *
 * @static
 * @memberOf _
 * @since 2.4.0
 * @category Date
 * @returns {number} Returns the timestamp.
 * @example
 *
 * _.defer(function(stamp) {
 *   console.log(_.now() - stamp);
 * }, _.now());
 * // => Logs the number of milliseconds it took for the deferred invocation.
 */
var now = function() {
  return root.Date.now();
};

module.exports = now;


/***/ }),

/***/ "499e":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-style-loader/lib/listToStyles.js
/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}

// CONCATENATED MODULE: ./node_modules/vue-style-loader/lib/addStylesClient.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return addStylesClient; });
/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/



var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}
var options = null
var ssrIdKey = 'data-vue-ssr-id'

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

function addStylesClient (parentId, list, _isProduction, _options) {
  isProduction = _isProduction

  options = _options || {}

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[' + ssrIdKey + '~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }
  if (options.ssrId) {
    styleElement.setAttribute(ssrIdKey, obj.id)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ "49ad":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function(idHandler) {
    var eventListeners = {};

    /**
     * Gets all listeners for the given element.
     * @public
     * @param {element} element The element to get all listeners for.
     * @returns All listeners for the given element.
     */
    function getListeners(element) {
        var id = idHandler.get(element);

        if (id === undefined) {
            return [];
        }

        return eventListeners[id] || [];
    }

    /**
     * Stores the given listener for the given element. Will not actually add the listener to the element.
     * @public
     * @param {element} element The element that should have the listener added.
     * @param {function} listener The callback that the element has added.
     */
    function addListener(element, listener) {
        var id = idHandler.get(element);

        if(!eventListeners[id]) {
            eventListeners[id] = [];
        }

        eventListeners[id].push(listener);
    }

    function removeListener(element, listener) {
        var listeners = getListeners(element);
        for (var i = 0, len = listeners.length; i < len; ++i) {
            if (listeners[i] === listener) {
              listeners.splice(i, 1);
              break;
            }
        }
    }

    function removeAllListeners(element) {
      var listeners = getListeners(element);
      if (!listeners) { return; }
      listeners.length = 0;
    }

    return {
        get: getListeners,
        add: addListener,
        removeListener: removeListener,
        removeAllListeners: removeAllListeners
    };
};


/***/ }),

/***/ "4a74":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _NetwalkGame_vue_vue_type_template_id_769a2275___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("323f");
/* harmony import */ var _NetwalkGame_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("5991");
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _NetwalkGame_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _NetwalkGame_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var _NetwalkGame_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("09f2");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("2877");








function injectStyles (context) {
  
  this["$style"] = (_NetwalkGame_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_2__["default"].locals || _NetwalkGame_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_2__["default"])

}

/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"])(
  _NetwalkGame_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _NetwalkGame_vue_vue_type_template_id_769a2275___WEBPACK_IMPORTED_MODULE_0__[/* render */ "a"],
  _NetwalkGame_vue_vue_type_template_id_769a2275___WEBPACK_IMPORTED_MODULE_0__[/* staticRenderFns */ "b"],
  false,
  injectStyles,
  null,
  null
  
)

component.options.__file = "NetwalkGame.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "4ec5":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("a68e");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("40c12eee", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "5058":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function(options) {
    var idGenerator     = options.idGenerator;
    var getState        = options.stateHandler.getState;

    /**
     * Gets the resize detector id of the element.
     * @public
     * @param {element} element The target element to get the id of.
     * @returns {string|number|null} The id of the element. Null if it has no id.
     */
    function getId(element) {
        var state = getState(element);

        if (state && state.id !== undefined) {
            return state.id;
        }

        return null;
    }

    /**
     * Sets the resize detector id of the element. Requires the element to have a resize detector state initialized.
     * @public
     * @param {element} element The target element to set the id of.
     * @returns {string|number|null} The id of the element.
     */
    function setId(element) {
        var state = getState(element);

        if (!state) {
            throw new Error("setId required the element to have a resize detection state.");
        }

        var id = idGenerator.generate();

        state.id = id;

        return id;
    }

    return {
        get: getId,
        set: setId
    };
};


/***/ }),

/***/ "50bf":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = module.exports = {};

utils.getOption = getOption;

function getOption(options, name, defaultValue) {
    var value = options[name];

    if((value === undefined || value === null) && defaultValue !== undefined) {
        return defaultValue;
    }

    return value;
}


/***/ }),

/***/ "585a":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {/** Detect free variable `global` from Node.js. */
var freeGlobal = typeof global == 'object' && global && global.Object === Object && global;

module.exports = freeGlobal;

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("c8ba")))

/***/ }),

/***/ "5991":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NetwalkGame_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("1743");
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NetwalkGame_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NetwalkGame_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NetwalkGame_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NetwalkGame_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NetwalkGame_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "5a83":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Cell_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("2d07");
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Cell_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Cell_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Cell_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Cell_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Cell_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "5be5":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function(options) {
    var getState = options.stateHandler.getState;

    /**
     * Tells if the element has been made detectable and ready to be listened for resize events.
     * @public
     * @param {element} The element to check.
     * @returns {boolean} True or false depending on if the element is detectable or not.
     */
    function isDetectable(element) {
        var state = getState(element);
        return state && !!state.isDetectable;
    }

    /**
     * Marks the element that it has been made detectable and ready to be listened for resize events.
     * @public
     * @param {element} The element to mark.
     */
    function markAsDetectable(element) {
        getState(element).isDetectable = true;
    }

    /**
     * Tells if the element is busy or not.
     * @public
     * @param {element} The element to check.
     * @returns {boolean} True or false depending on if the element is busy or not.
     */
    function isBusy(element) {
        return !!getState(element).busy;
    }

    /**
     * Marks the object is busy and should not be made detectable.
     * @public
     * @param {element} element The element to mark.
     * @param {boolean} busy If the element is busy or not.
     */
    function markBusy(element, busy) {
        getState(element).busy = !!busy;
    }

    return {
        isDetectable: isDetectable,
        markAsDetectable: markAsDetectable,
        isBusy: isBusy,
        markBusy: markBusy
    };
};


/***/ }),

/***/ "81f8":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"2867f158-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/PlayingField.vue?vue&type=template&id=dd8085ea&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{class:_vm.$style.container},_vm._l((_vm.matrix),function(row,rowIndex){return _c('div',{key:("row-" + rowIndex),class:_vm.$style.row},_vm._l((row),function(cell){return _c('cell',{key:("cell-" + (cell.id)),attrs:{"style-vars":_vm.styleVars,"type":cell.type,"connections":cell.connections,"connected":cell.connected},on:{"rotate-node":function($event){_vm.$emit('rotate-node', cell.id)}}})}))}))}
var staticRenderFns = []


// CONCATENATED MODULE: ./src/components/PlayingField.vue?vue&type=template&id=dd8085ea&
/* concated harmony reexport render */__webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* concated harmony reexport staticRenderFns */__webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });


/***/ }),

/***/ "8943":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayingField_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("89dd");
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayingField_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayingField_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayingField_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayingField_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PlayingField_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "89dd":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _Cell = _interopRequireDefault(__webpack_require__("b1ea"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
var _default = {
  name: 'PlayingField',
  components: {
    'cell': _Cell.default
  },
  props: {
    matrix: {
      type: Array,
      required: true
    },
    styleVars: {
      type: Object,
      required: true
    }
  }
};
exports.default = _default;

/***/ }),

/***/ "91ce":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("c413");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("b3e2884c", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "91ea":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"2867f158-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Cell.vue?vue&type=template&id=3183297c&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{class:[_vm.$style.cell, ( _obj = {}, _obj[_vm.$style['cell--connected']] = _vm.connected, _obj )],style:({
    width: ((_vm.styleVars.cellSizePx) + "px"),
    height: ((_vm.styleVars.cellSizePx) + "px"),
    backgroundColor: ("" + (_vm.type !== 'empty' ? _vm.styleVars.cellColor : _vm.styleVars.emptyCellColor)),

  })},[(_vm.connections.length > 0)?_c('svg',{class:[_vm.$style.node],attrs:{"version":"1.1","xmlns":"http://www.w3.org/2000/svg","preserveAspectRatio":"xMidYMid"}},[_c('g',{class:_vm.$style.connections,style:({
      fill: _vm.connected ? _vm.styleVars.connectionColorConnected : _vm.styleVars.connectionColor,
      transition: _vm.connectionsTransition,
      transform: ("rotate(" + _vm.rotationAngle + "deg)"),
      })},[(_vm.connectionParams.type === 'half' || _vm.connectionParams.type === 'elbow')?_c('rect',{attrs:{"height":((50 + (_vm.styleVars.connectionWidthPercent / 2)) + "%"),"width":((_vm.styleVars.connectionWidthPercent) + "%"),"x":((50 - (_vm.styleVars.connectionWidthPercent / 2)) + "%")}}):_vm._e(),(_vm.connectionParams.type === 'tee' || _vm.connectionParams.type === 'elbow')?_c('rect',{attrs:{"height":((_vm.styleVars.connectionWidthPercent) + "%"),"width":((50 + (_vm.styleVars.connectionWidthPercent / 2)) + "%"),"y":((50 - (_vm.styleVars.connectionWidthPercent / 2)) + "%"),"x":((50 - (_vm.styleVars.connectionWidthPercent / 2)) + "%")}}):_vm._e(),(_vm.connectionParams.type === 'straight' || _vm.connectionParams.type === 'tee' || _vm.connectionParams.type === 'crosshair')?_c('rect',{attrs:{"height":"100%","width":((_vm.styleVars.connectionWidthPercent) + "%"),"x":((50 - (_vm.styleVars.connectionWidthPercent / 2)) + "%")}}):_vm._e(),(_vm.connectionParams.type === 'crosshair')?_c('rect',{attrs:{"width":"100%","height":((_vm.styleVars.connectionWidthPercent) + "%"),"y":((50 - (_vm.styleVars.connectionWidthPercent / 2)) + "%")}}):_vm._e()])]):_vm._e(),(_vm.type === 'provider')?_c('div',{class:_vm.$style.provider,style:({
    backgroundColor: _vm.styleVars.providerColor
    })}):_vm._e(),(_vm.type === 'consumer')?_c('div',{class:_vm.$style.consumer,style:({
    transition: _vm.consumerTransition,
    backgroundColor: _vm.connected ? _vm.styleVars.consumerColorConnected : _vm.styleVars.consumerColor
    })}):_vm._e(),(_vm.connections.length > 0 && _vm.connections.length < 4)?_c('div',{class:_vm.$style['rotate-layer'],on:{"click":function($event){_vm.$emit('rotate-node')}}}):_vm._e()])
var _obj;}
var staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Cell.vue?vue&type=template&id=3183297c&
/* concated harmony reexport render */__webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* concated harmony reexport staticRenderFns */__webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });


/***/ }),

/***/ "92e8":
/***/ (function(module, exports, __webpack_require__) {

!function(t,n){ true?module.exports=n():undefined}(window,function(){return function(t){var n={};function e(r){if(n[r])return n[r].exports;var o=n[r]={i:r,l:!1,exports:{}};return t[r].call(o.exports,o,o.exports,e),o.l=!0,o.exports}return e.m=t,e.c=n,e.d=function(t,n,r){e.o(t,n)||Object.defineProperty(t,n,{enumerable:!0,get:r})},e.r=function(t){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(t,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(t,"__esModule",{value:!0})},e.t=function(t,n){if(1&n&&(t=e(t)),8&n)return t;if(4&n&&"object"==typeof t&&t&&t.__esModule)return t;var r=Object.create(null);if(e.r(r),Object.defineProperty(r,"default",{enumerable:!0,value:t}),2&n&&"string"!=typeof t)for(var o in t)e.d(r,o,function(n){return t[n]}.bind(null,o));return r},e.n=function(t){var n=t&&t.__esModule?function(){return t.default}:function(){return t};return e.d(n,"a",n),n},e.o=function(t,n){return Object.prototype.hasOwnProperty.call(t,n)},e.p="",e(e.s=223)}([function(t,n,e){var r=e(45)("wks"),o=e(19),i=e(2).Symbol,a="function"==typeof i;(t.exports=function(t){return r[t]||(r[t]=a&&i[t]||(a?i:o)("Symbol."+t))}).store=r},function(t,n,e){var r=e(2),o=e(9),i=e(10),a=e(11),u=e(13),c=function(t,n,e){var s,f,l,h,p=t&c.F,v=t&c.G,y=t&c.S,d=t&c.P,m=t&c.B,g=v?r:y?r[n]||(r[n]={}):(r[n]||{}).prototype,x=v?o:o[n]||(o[n]={}),b=x.prototype||(x.prototype={});for(s in v&&(e=n),e)l=((f=!p&&g&&void 0!==g[s])?g:e)[s],h=m&&f?u(l,r):d&&"function"==typeof l?u(Function.call,l):l,g&&a(g,s,l,t&c.U),x[s]!=l&&i(x,s,h),d&&b[s]!=l&&(b[s]=l)};r.core=o,c.F=1,c.G=2,c.S=4,c.P=8,c.B=16,c.W=32,c.U=64,c.R=128,t.exports=c},function(t,n){var e=t.exports="undefined"!=typeof window&&window.Math==Math?window:"undefined"!=typeof self&&self.Math==Math?self:Function("return this")();"number"==typeof __g&&(__g=e)},function(t,n,e){var r=e(4),o=e(57),i=e(43),a=Object.defineProperty;n.f=e(5)?Object.defineProperty:function(t,n,e){if(r(t),n=i(n,!0),r(e),o)try{return a(t,n,e)}catch(t){}if("get"in e||"set"in e)throw TypeError("Accessors not supported!");return"value"in e&&(t[n]=e.value),t}},function(t,n,e){var r=e(6);t.exports=function(t){if(!r(t))throw TypeError(t+" is not an object!");return t}},function(t,n,e){t.exports=!e(8)(function(){return 7!=Object.defineProperty({},"a",{get:function(){return 7}}).a})},function(t,n){t.exports=function(t){return"object"==typeof t?null!==t:"function"==typeof t}},function(t,n){function e(t,n){if(!(this instanceof e))return new e(t,n);this.x=t||0,this.y=n||0}t.exports=e,e.fromArray=function(t){return new e(t[0]||0,t[1]||0)},e.fromObject=function(t){return new e(t.x||0,t.y||0)},e.prototype.addX=function(t){return this.x+=t.x,this},e.prototype.addY=function(t){return this.y+=t.y,this},e.prototype.add=function(t){return this.x+=t.x,this.y+=t.y,this},e.prototype.addScalar=function(t){return this.x+=t,this.y+=t,this},e.prototype.addScalarX=function(t){return this.x+=t,this},e.prototype.addScalarY=function(t){return this.y+=t,this},e.prototype.subtractX=function(t){return this.x-=t.x,this},e.prototype.subtractY=function(t){return this.y-=t.y,this},e.prototype.subtract=function(t){return this.x-=t.x,this.y-=t.y,this},e.prototype.subtractScalar=function(t){return this.x-=t,this.y-=t,this},e.prototype.subtractScalarX=function(t){return this.x-=t,this},e.prototype.subtractScalarY=function(t){return this.y-=t,this},e.prototype.divideX=function(t){return this.x/=t.x,this},e.prototype.divideY=function(t){return this.y/=t.y,this},e.prototype.divide=function(t){return this.x/=t.x,this.y/=t.y,this},e.prototype.divideScalar=function(t){return 0!==t?(this.x/=t,this.y/=t):(this.x=0,this.y=0),this},e.prototype.divideScalarX=function(t){return 0!==t?this.x/=t:this.x=0,this},e.prototype.divideScalarY=function(t){return 0!==t?this.y/=t:this.y=0,this},e.prototype.invertX=function(){return this.x*=-1,this},e.prototype.invertY=function(){return this.y*=-1,this},e.prototype.invert=function(){return this.invertX(),this.invertY(),this},e.prototype.multiplyX=function(t){return this.x*=t.x,this},e.prototype.multiplyY=function(t){return this.y*=t.y,this},e.prototype.multiply=function(t){return this.x*=t.x,this.y*=t.y,this},e.prototype.multiplyScalar=function(t){return this.x*=t,this.y*=t,this},e.prototype.multiplyScalarX=function(t){return this.x*=t,this},e.prototype.multiplyScalarY=function(t){return this.y*=t,this},e.prototype.normalize=function(){var t=this.length();return 0===t?(this.x=1,this.y=0):this.divide(e(t,t)),this},e.prototype.norm=e.prototype.normalize,e.prototype.limit=function(t,n){return Math.abs(this.x)>t&&(this.x*=n),Math.abs(this.y)>t&&(this.y*=n),this},e.prototype.randomize=function(t,n){return this.randomizeX(t,n),this.randomizeY(t,n),this},e.prototype.randomizeX=function(t,n){var e=Math.min(t.x,n.x),r=Math.max(t.x,n.x);return this.x=o(e,r),this},e.prototype.randomizeY=function(t,n){var e=Math.min(t.y,n.y),r=Math.max(t.y,n.y);return this.y=o(e,r),this},e.prototype.randomizeAny=function(t,n){return Math.round(Math.random())?this.randomizeX(t,n):this.randomizeY(t,n),this},e.prototype.unfloat=function(){return this.x=Math.round(this.x),this.y=Math.round(this.y),this},e.prototype.toFixed=function(t){return void 0===t&&(t=8),this.x=this.x.toFixed(t),this.y=this.y.toFixed(t),this},e.prototype.mixX=function(t,n){return void 0===n&&(n=.5),this.x=(1-n)*this.x+n*t.x,this},e.prototype.mixY=function(t,n){return void 0===n&&(n=.5),this.y=(1-n)*this.y+n*t.y,this},e.prototype.mix=function(t,n){return this.mixX(t,n),this.mixY(t,n),this},e.prototype.clone=function(){return new e(this.x,this.y)},e.prototype.copyX=function(t){return this.x=t.x,this},e.prototype.copyY=function(t){return this.y=t.y,this},e.prototype.copy=function(t){return this.copyX(t),this.copyY(t),this},e.prototype.zero=function(){return this.x=this.y=0,this},e.prototype.dot=function(t){return this.x*t.x+this.y*t.y},e.prototype.cross=function(t){return this.x*t.y-this.y*t.x},e.prototype.projectOnto=function(t){var n=(this.x*t.x+this.y*t.y)/(t.x*t.x+t.y*t.y);return this.x=n*t.x,this.y=n*t.y,this},e.prototype.horizontalAngle=function(){return Math.atan2(this.y,this.x)},e.prototype.horizontalAngleDeg=function(){return i(this.horizontalAngle())},e.prototype.verticalAngle=function(){return Math.atan2(this.x,this.y)},e.prototype.verticalAngleDeg=function(){return i(this.verticalAngle())},e.prototype.angle=e.prototype.horizontalAngle,e.prototype.angleDeg=e.prototype.horizontalAngleDeg,e.prototype.direction=e.prototype.horizontalAngle,e.prototype.rotate=function(t){var n=this.x*Math.cos(t)-this.y*Math.sin(t),e=this.x*Math.sin(t)+this.y*Math.cos(t);return this.x=n,this.y=e,this},e.prototype.rotateDeg=function(t){return t=a(t),this.rotate(t)},e.prototype.rotateTo=function(t){return this.rotate(t-this.angle())},e.prototype.rotateToDeg=function(t){return t=a(t),this.rotateTo(t)},e.prototype.rotateBy=function(t){var n=this.angle()+t;return this.rotate(n)},e.prototype.rotateByDeg=function(t){return t=a(t),this.rotateBy(t)},e.prototype.distanceX=function(t){return this.x-t.x},e.prototype.absDistanceX=function(t){return Math.abs(this.distanceX(t))},e.prototype.distanceY=function(t){return this.y-t.y},e.prototype.absDistanceY=function(t){return Math.abs(this.distanceY(t))},e.prototype.distance=function(t){return Math.sqrt(this.distanceSq(t))},e.prototype.distanceSq=function(t){var n=this.distanceX(t),e=this.distanceY(t);return n*n+e*e},e.prototype.length=function(){return Math.sqrt(this.lengthSq())},e.prototype.lengthSq=function(){return this.x*this.x+this.y*this.y},e.prototype.magnitude=e.prototype.length,e.prototype.isZero=function(){return 0===this.x&&0===this.y},e.prototype.isEqualTo=function(t){return this.x===t.x&&this.y===t.y},e.prototype.toString=function(){return"x:"+this.x+", y:"+this.y},e.prototype.toArray=function(){return[this.x,this.y]},e.prototype.toObject=function(){return{x:this.x,y:this.y}};var r=180/Math.PI;function o(t,n){return Math.floor(Math.random()*(n-t+1)+t)}function i(t){return t*r}function a(t){return t/r}},function(t,n){t.exports=function(t){try{return!!t()}catch(t){return!0}}},function(t,n){var e=t.exports={version:"2.5.7"};"number"==typeof __e&&(__e=e)},function(t,n,e){var r=e(3),o=e(18);t.exports=e(5)?function(t,n,e){return r.f(t,n,o(1,e))}:function(t,n,e){return t[n]=e,t}},function(t,n,e){var r=e(2),o=e(10),i=e(12),a=e(19)("src"),u=Function.toString,c=(""+u).split("toString");e(9).inspectSource=function(t){return u.call(t)},(t.exports=function(t,n,e,u){var s="function"==typeof e;s&&(i(e,"name")||o(e,"name",n)),t[n]!==e&&(s&&(i(e,a)||o(e,a,t[n]?""+t[n]:c.join(String(n)))),t===r?t[n]=e:u?t[n]?t[n]=e:o(t,n,e):(delete t[n],o(t,n,e)))})(Function.prototype,"toString",function(){return"function"==typeof this&&this[a]||u.call(this)})},function(t,n){var e={}.hasOwnProperty;t.exports=function(t,n){return e.call(t,n)}},function(t,n,e){var r=e(28);t.exports=function(t,n,e){if(r(t),void 0===n)return t;switch(e){case 1:return function(e){return t.call(n,e)};case 2:return function(e,r){return t.call(n,e,r)};case 3:return function(e,r,o){return t.call(n,e,r,o)}}return function(){return t.apply(n,arguments)}}},function(t,n,e){var r=e(46),o=e(29);t.exports=function(t){return r(o(t))}},function(t,n){var e={}.toString;t.exports=function(t){return e.call(t).slice(8,-1)}},function(t,n,e){var r=e(62),o=e(49);t.exports=Object.keys||function(t){return r(t,o)}},function(t,n,e){var r=e(29);t.exports=function(t){return Object(r(t))}},function(t,n){t.exports=function(t,n){return{enumerable:!(1&t),configurable:!(2&t),writable:!(4&t),value:n}}},function(t,n){var e=0,r=Math.random();t.exports=function(t){return"Symbol(".concat(void 0===t?"":t,")_",(++e+r).toString(36))}},function(t,n){t.exports=!1},function(t,n){t.exports={}},function(t,n,e){var r=e(47),o=Math.min;t.exports=function(t){return t>0?o(r(t),9007199254740991):0}},function(t,n,e){var r=e(69),o="object"==typeof self&&self&&self.Object===Object&&self,i=r||o||Function("return this")();t.exports=i},function(t,n){t.exports=function(t){return null!=t&&"object"==typeof t}},function(t,n){var e=Array.isArray;t.exports=e},function(t,n,e){var r=e(104),o=e(218),i=e(25);t.exports=function(t){return(i(t)?r:o)(t)}},function(t,n,e){var r=e(1);r(r.S+r.F*!e(5),"Object",{defineProperty:e(3).f})},function(t,n){t.exports=function(t){if("function"!=typeof t)throw TypeError(t+" is not a function!");return t}},function(t,n){t.exports=function(t){if(void 0==t)throw TypeError("Can't call method on  "+t);return t}},function(t,n,e){var r=e(3).f,o=e(12),i=e(0)("toStringTag");t.exports=function(t,n,e){t&&!o(t=e?t:t.prototype,i)&&r(t,i,{configurable:!0,value:n})}},function(t,n,e){var r=e(13),o=e(46),i=e(17),a=e(22),u=e(115);t.exports=function(t,n){var e=1==t,c=2==t,s=3==t,f=4==t,l=6==t,h=5==t||l,p=n||u;return function(n,u,v){for(var y,d,m=i(n),g=o(m),x=r(u,v,3),b=a(g.length),w=0,C=e?p(n,b):c?p(n,0):void 0;b>w;w++)if((h||w in g)&&(d=x(y=g[w],w,m),t))if(e)C[w]=d;else if(d)switch(t){case 3:return!0;case 5:return y;case 6:return w;case 2:C.push(y)}else if(f)return!1;return l?-1:s||f?f:C}}},function(t,n,e){"use strict";var r=e(8);t.exports=function(t,n){return!!t&&r(function(){n?t.call(null,function(){},1):t.call(null)})}},function(t,n,e){var r=e(34)(Object,"create");t.exports=r},function(t,n,e){var r=e(122),o=e(129);t.exports=function(t,n){var e=o(t,n);return r(e)?e:void 0}},function(t,n,e){var r=e(36),o=e(124),i=e(125),a="[object Null]",u="[object Undefined]",c=r?r.toStringTag:void 0;t.exports=function(t){return null==t?void 0===t?u:a:c&&c in Object(t)?o(t):i(t)}},function(t,n,e){var r=e(23).Symbol;t.exports=r},function(t,n){t.exports=function(t){var n=typeof t;return null!=t&&("object"==n||"function"==n)}},function(t,n,e){var r=e(70);t.exports=function(t,n){for(var e=t.length;e--;)if(r(t[e][0],n))return e;return-1}},function(t,n,e){var r=e(142);t.exports=function(t,n){var e=t.__data__;return r(n)?e["string"==typeof n?"string":"hash"]:e.map}},function(t,n){n.f={}.propertyIsEnumerable},function(t,n,e){var r=e(55),o=e(94),i=e(96),a=parseFloat,u=Math.min,c=Math.random;t.exports=function(t,n,e){if(e&&"boolean"!=typeof e&&o(t,n,e)&&(n=e=void 0),void 0===e&&("boolean"==typeof n?(e=n,n=void 0):"boolean"==typeof t&&(e=t,t=void 0)),void 0===t&&void 0===n?(t=0,n=1):(t=i(t),void 0===n?(n=t,t=0):n=i(n)),t>n){var s=t;t=n,n=s}if(e||t%1||n%1){var f=c();return u(t+f*(n-t+a("1e-"+((f+"").length-1))),n)}return r(t,n)}},function(t,n,e){var r=e(6),o=e(2).document,i=r(o)&&r(o.createElement);t.exports=function(t){return i?o.createElement(t):{}}},function(t,n,e){var r=e(6);t.exports=function(t,n){if(!r(t))return t;var e,o;if(n&&"function"==typeof(e=t.toString)&&!r(o=e.call(t)))return o;if("function"==typeof(e=t.valueOf)&&!r(o=e.call(t)))return o;if(!n&&"function"==typeof(e=t.toString)&&!r(o=e.call(t)))return o;throw TypeError("Can't convert object to primitive value")}},function(t,n,e){var r=e(0)("unscopables"),o=Array.prototype;void 0==o[r]&&e(10)(o,r,{}),t.exports=function(t){o[r][t]=!0}},function(t,n,e){var r=e(9),o=e(2),i=o["__core-js_shared__"]||(o["__core-js_shared__"]={});(t.exports=function(t,n){return i[t]||(i[t]=void 0!==n?n:{})})("versions",[]).push({version:r.version,mode:e(20)?"pure":"global",copyright:"© 2018 Denis Pushkarev (zloirock.ru)"})},function(t,n,e){var r=e(15);t.exports=Object("z").propertyIsEnumerable(0)?Object:function(t){return"String"==r(t)?t.split(""):Object(t)}},function(t,n){var e=Math.ceil,r=Math.floor;t.exports=function(t){return isNaN(t=+t)?0:(t>0?r:e)(t)}},function(t,n,e){var r=e(45)("keys"),o=e(19);t.exports=function(t){return r[t]||(r[t]=o(t))}},function(t,n){t.exports="constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(",")},function(t,n,e){var r=e(15);t.exports=Array.isArray||function(t){return"Array"==r(t)}},function(t,n){t.exports=function(t,n){for(var e=-1,r=null==t?0:t.length,o=Array(r);++e<r;)o[e]=n(t[e],e,t);return o}},function(t,n){t.exports=function(t){return t}},function(t,n,e){var r=e(68),o=e(79);t.exports=function(t){return null!=t&&o(t.length)&&!r(t)}},function(t,n){n.f=Object.getOwnPropertySymbols},function(t,n){var e=Math.floor,r=Math.random;t.exports=function(t,n){return t+e(r()*(n-t+1))}},function(t,n,e){var r=e(75),o=e(77),i=e(199),a=e(78),u=o(function(t){return i(r(t,1,a,!0))});t.exports=u},function(t,n,e){t.exports=!e(5)&&!e(8)(function(){return 7!=Object.defineProperty(e(42)("div"),"a",{get:function(){return 7}}).a})},function(t,n,e){for(var r=e(59),o=e(16),i=e(11),a=e(2),u=e(10),c=e(21),s=e(0),f=s("iterator"),l=s("toStringTag"),h=c.Array,p={CSSRuleList:!0,CSSStyleDeclaration:!1,CSSValueList:!1,ClientRectList:!1,DOMRectList:!1,DOMStringList:!1,DOMTokenList:!0,DataTransferItemList:!1,FileList:!1,HTMLAllCollection:!1,HTMLCollection:!1,HTMLFormElement:!1,HTMLSelectElement:!1,MediaList:!0,MimeTypeArray:!1,NamedNodeMap:!1,NodeList:!0,PaintRequestList:!1,Plugin:!1,PluginArray:!1,SVGLengthList:!1,SVGNumberList:!1,SVGPathSegList:!1,SVGPointList:!1,SVGStringList:!1,SVGTransformList:!1,SourceBufferList:!1,StyleSheetList:!0,TextTrackCueList:!1,TextTrackList:!1,TouchList:!1},v=o(p),y=0;y<v.length;y++){var d,m=v[y],g=p[m],x=a[m],b=x&&x.prototype;if(b&&(b[f]||u(b,f,h),b[l]||u(b,l,m),c[m]=h,g))for(d in r)b[d]||i(b,d,r[d],!0)}},function(t,n,e){"use strict";var r=e(44),o=e(109),i=e(21),a=e(14);t.exports=e(60)(Array,"Array",function(t,n){this._t=a(t),this._i=0,this._k=n},function(){var t=this._t,n=this._k,e=this._i++;return!t||e>=t.length?(this._t=void 0,o(1)):o(0,"keys"==n?e:"values"==n?t[e]:[e,t[e]])},"values"),i.Arguments=i.Array,r("keys"),r("values"),r("entries")},function(t,n,e){"use strict";var r=e(20),o=e(1),i=e(11),a=e(10),u=e(21),c=e(110),s=e(30),f=e(112),l=e(0)("iterator"),h=!([].keys&&"next"in[].keys()),p=function(){return this};t.exports=function(t,n,e,v,y,d,m){c(e,n,v);var g,x,b,w=function(t){if(!h&&t in S)return S[t];switch(t){case"keys":case"values":return function(){return new e(this,t)}}return function(){return new e(this,t)}},C=n+" Iterator",_="values"==y,k=!1,S=t.prototype,j=S[l]||S["@@iterator"]||y&&S[y],O=j||w(y),A=y?_?w("entries"):O:void 0,P="Array"==n&&S.entries||j;if(P&&(b=f(P.call(new t)))!==Object.prototype&&b.next&&(s(b,C,!0),r||"function"==typeof b[l]||a(b,l,p)),_&&j&&"values"!==j.name&&(k=!0,O=function(){return j.call(this)}),r&&!m||!h&&!k&&S[l]||a(S,l,O),u[n]=O,u[C]=p,y)if(g={values:_?O:w("values"),keys:d?O:w("keys"),entries:A},m)for(x in g)x in S||i(S,x,g[x]);else o(o.P+o.F*(h||k),n,g);return g}},function(t,n,e){var r=e(4),o=e(111),i=e(49),a=e(48)("IE_PROTO"),u=function(){},c=function(){var t,n=e(42)("iframe"),r=i.length;for(n.style.display="none",e(65).appendChild(n),n.src="javascript:",(t=n.contentWindow.document).open(),t.write("<script>document.F=Object<\/script>"),t.close(),c=t.F;r--;)delete c.prototype[i[r]];return c()};t.exports=Object.create||function(t,n){var e;return null!==t?(u.prototype=r(t),e=new u,u.prototype=null,e[a]=t):e=c(),void 0===n?e:o(e,n)}},function(t,n,e){var r=e(12),o=e(14),i=e(63)(!1),a=e(48)("IE_PROTO");t.exports=function(t,n){var e,u=o(t),c=0,s=[];for(e in u)e!=a&&r(u,e)&&s.push(e);for(;n.length>c;)r(u,e=n[c++])&&(~i(s,e)||s.push(e));return s}},function(t,n,e){var r=e(14),o=e(22),i=e(64);t.exports=function(t){return function(n,e,a){var u,c=r(n),s=o(c.length),f=i(a,s);if(t&&e!=e){for(;s>f;)if((u=c[f++])!=u)return!0}else for(;s>f;f++)if((t||f in c)&&c[f]===e)return t||f||0;return!t&&-1}}},function(t,n,e){var r=e(47),o=Math.max,i=Math.min;t.exports=function(t,n){return(t=r(t))<0?o(t+n,0):i(t,n)}},function(t,n,e){var r=e(2).document;t.exports=r&&r.documentElement},function(t,n,e){"use strict";var r=e(1),o=e(31)(1);r(r.P+r.F*!e(32)([].map,!0),"Array",{map:function(t){return o(this,t,arguments[1])}})},function(t,n,e){var r=e(118),o=e(146),i=e(147);function a(t){var n=-1,e=null==t?0:t.length;for(this.__data__=new r;++n<e;)this.add(t[n])}a.prototype.add=a.prototype.push=o,a.prototype.has=i,t.exports=a},function(t,n,e){var r=e(35),o=e(37),i="[object AsyncFunction]",a="[object Function]",u="[object GeneratorFunction]",c="[object Proxy]";t.exports=function(t){if(!o(t))return!1;var n=r(t);return n==a||n==u||n==i||n==c}},function(t,n,e){(function(n){var e="object"==typeof n&&n&&n.Object===Object&&n;t.exports=e}).call(this,e(123))},function(t,n){t.exports=function(t,n){return t===n||t!=t&&n!=n}},function(t,n,e){var r=e(148);t.exports=function(t,n){return!(null==t||!t.length)&&r(t,n,0)>-1}},function(t,n){t.exports=function(t,n,e){for(var r=-1,o=null==t?0:t.length;++r<o;)if(e(n,t[r]))return!0;return!1}},function(t,n){t.exports=function(t){return function(n){return t(n)}}},function(t,n){t.exports=function(t,n){return t.has(n)}},function(t,n,e){var r=e(152),o=e(153);t.exports=function t(n,e,i,a,u){var c=-1,s=n.length;for(i||(i=o),u||(u=[]);++c<s;){var f=n[c];e>0&&i(f)?e>1?t(f,e-1,i,a,u):r(u,f):a||(u[u.length]=f)}return u}},function(t,n,e){var r=e(154),o=e(24),i=Object.prototype,a=i.hasOwnProperty,u=i.propertyIsEnumerable,c=r(function(){return arguments}())?r:function(t){return o(t)&&a.call(t,"callee")&&!u.call(t,"callee")};t.exports=c},function(t,n,e){var r=e(52),o=e(155),i=e(157);t.exports=function(t,n){return i(o(t,n,r),t+"")}},function(t,n,e){var r=e(53),o=e(24);t.exports=function(t){return o(t)&&r(t)}},function(t,n){var e=9007199254740991;t.exports=function(t){return"number"==typeof t&&t>-1&&t%1==0&&t<=e}},function(t,n,e){var r=e(35),o=e(24),i="[object Symbol]";t.exports=function(t){return"symbol"==typeof t||o(t)&&r(t)==i}},function(t,n,e){"use strict";var r=e(4);t.exports=function(){var t=r(this),n="";return t.global&&(n+="g"),t.ignoreCase&&(n+="i"),t.multiline&&(n+="m"),t.unicode&&(n+="u"),t.sticky&&(n+="y"),n}},function(t,n,e){"use strict";var r,o,i,a,u=e(20),c=e(2),s=e(13),f=e(83),l=e(1),h=e(6),p=e(28),v=e(167),y=e(168),d=e(169),m=e(87).set,g=e(171)(),x=e(88),b=e(172),w=e(173),C=e(174),_=c.TypeError,k=c.process,S=k&&k.versions,j=S&&S.v8||"",O=c.Promise,A="process"==f(k),P=function(){},E=o=x.f,R=!!function(){try{var t=O.resolve(1),n=(t.constructor={})[e(0)("species")]=function(t){t(P,P)};return(A||"function"==typeof PromiseRejectionEvent)&&t.then(P)instanceof n&&0!==j.indexOf("6.6")&&-1===w.indexOf("Chrome/66")}catch(t){}}(),T=function(t){var n;return!(!h(t)||"function"!=typeof(n=t.then))&&n},M=function(t,n){if(!t._n){t._n=!0;var e=t._c;g(function(){for(var r=t._v,o=1==t._s,i=0,a=function(n){var e,i,a,u=o?n.ok:n.fail,c=n.resolve,s=n.reject,f=n.domain;try{u?(o||(2==t._h&&F(t),t._h=1),!0===u?e=r:(f&&f.enter(),e=u(r),f&&(f.exit(),a=!0)),e===n.promise?s(_("Promise-chain cycle")):(i=T(e))?i.call(e,c,s):c(e)):s(r)}catch(t){f&&!a&&f.exit(),s(t)}};e.length>i;)a(e[i++]);t._c=[],t._n=!1,n&&!t._h&&z(t)})}},z=function(t){m.call(c,function(){var n,e,r,o=t._v,i=N(t);if(i&&(n=b(function(){A?k.emit("unhandledRejection",o,t):(e=c.onunhandledrejection)?e({promise:t,reason:o}):(r=c.console)&&r.error&&r.error("Unhandled promise rejection",o)}),t._h=A||N(t)?2:1),t._a=void 0,i&&n.e)throw n.v})},N=function(t){return 1!==t._h&&0===(t._a||t._c).length},F=function(t){m.call(c,function(){var n;A?k.emit("rejectionHandled",t):(n=c.onrejectionhandled)&&n({promise:t,reason:t._v})})},I=function(t){var n=this;n._d||(n._d=!0,(n=n._w||n)._v=t,n._s=2,n._a||(n._a=n._c.slice()),M(n,!0))},L=function(t){var n,e=this;if(!e._d){e._d=!0,e=e._w||e;try{if(e===t)throw _("Promise can't be resolved itself");(n=T(t))?g(function(){var r={_w:e,_d:!1};try{n.call(t,s(L,r,1),s(I,r,1))}catch(t){I.call(r,t)}}):(e._v=t,e._s=1,M(e,!1))}catch(t){I.call({_w:e,_d:!1},t)}}};R||(O=function(t){v(this,O,"Promise","_h"),p(t),r.call(this);try{t(s(L,this,1),s(I,this,1))}catch(t){I.call(this,t)}},(r=function(t){this._c=[],this._a=void 0,this._s=0,this._d=!1,this._v=void 0,this._h=0,this._n=!1}).prototype=e(175)(O.prototype,{then:function(t,n){var e=E(d(this,O));return e.ok="function"!=typeof t||t,e.fail="function"==typeof n&&n,e.domain=A?k.domain:void 0,this._c.push(e),this._a&&this._a.push(e),this._s&&M(this,!1),e.promise},catch:function(t){return this.then(void 0,t)}}),i=function(){var t=new r;this.promise=t,this.resolve=s(L,t,1),this.reject=s(I,t,1)},x.f=E=function(t){return t===O||t===a?new i(t):o(t)}),l(l.G+l.W+l.F*!R,{Promise:O}),e(30)(O,"Promise"),e(176)("Promise"),a=e(9).Promise,l(l.S+l.F*!R,"Promise",{reject:function(t){var n=E(this);return(0,n.reject)(t),n.promise}}),l(l.S+l.F*(u||!R),"Promise",{resolve:function(t){return C(u&&this===a?O:this,t)}}),l(l.S+l.F*!(R&&e(89)(function(t){O.all(t).catch(P)})),"Promise",{all:function(t){var n=this,e=E(n),r=e.resolve,o=e.reject,i=b(function(){var e=[],i=0,a=1;y(t,!1,function(t){var u=i++,c=!1;e.push(void 0),a++,n.resolve(t).then(function(t){c||(c=!0,e[u]=t,--a||r(e))},o)}),--a||r(e)});return i.e&&o(i.v),e.promise},race:function(t){var n=this,e=E(n),r=e.reject,o=b(function(){y(t,!1,function(t){n.resolve(t).then(e.resolve,r)})});return o.e&&r(o.v),e.promise}})},function(t,n,e){var r=e(15),o=e(0)("toStringTag"),i="Arguments"==r(function(){return arguments}());t.exports=function(t){var n,e,a;return void 0===t?"Undefined":null===t?"Null":"string"==typeof(e=function(t,n){try{return t[n]}catch(t){}}(n=Object(t),o))?e:i?r(n):"Object"==(a=r(n))&&"function"==typeof n.callee?"Arguments":a}},function(t,n,e){var r=e(4);t.exports=function(t,n,e,o){try{return o?n(r(e)[0],e[1]):n(e)}catch(n){var i=t.return;throw void 0!==i&&r(i.call(t)),n}}},function(t,n,e){var r=e(21),o=e(0)("iterator"),i=Array.prototype;t.exports=function(t){return void 0!==t&&(r.Array===t||i[o]===t)}},function(t,n,e){var r=e(83),o=e(0)("iterator"),i=e(21);t.exports=e(9).getIteratorMethod=function(t){if(void 0!=t)return t[o]||t["@@iterator"]||i[r(t)]}},function(t,n,e){var r,o,i,a=e(13),u=e(170),c=e(65),s=e(42),f=e(2),l=f.process,h=f.setImmediate,p=f.clearImmediate,v=f.MessageChannel,y=f.Dispatch,d=0,m={},g=function(){var t=+this;if(m.hasOwnProperty(t)){var n=m[t];delete m[t],n()}},x=function(t){g.call(t.data)};h&&p||(h=function(t){for(var n=[],e=1;arguments.length>e;)n.push(arguments[e++]);return m[++d]=function(){u("function"==typeof t?t:Function(t),n)},r(d),d},p=function(t){delete m[t]},"process"==e(15)(l)?r=function(t){l.nextTick(a(g,t,1))}:y&&y.now?r=function(t){y.now(a(g,t,1))}:v?(i=(o=new v).port2,o.port1.onmessage=x,r=a(i.postMessage,i,1)):f.addEventListener&&"function"==typeof postMessage&&!f.importScripts?(r=function(t){f.postMessage(t+"","*")},f.addEventListener("message",x,!1)):r="onreadystatechange"in s("script")?function(t){c.appendChild(s("script")).onreadystatechange=function(){c.removeChild(this),g.call(t)}}:function(t){setTimeout(a(g,t,1),0)}),t.exports={set:h,clear:p}},function(t,n,e){"use strict";var r=e(28);t.exports.f=function(t){return new function(t){var n,e;this.promise=new t(function(t,r){if(void 0!==n||void 0!==e)throw TypeError("Bad Promise constructor");n=t,e=r}),this.resolve=r(n),this.reject=r(e)}(t)}},function(t,n,e){var r=e(0)("iterator"),o=!1;try{var i=[7][r]();i.return=function(){o=!0},Array.from(i,function(){throw 2})}catch(t){}t.exports=function(t,n){if(!n&&!o)return!1;var e=!1;try{var i=[7],a=i[r]();a.next=function(){return{done:e=!0}},i[r]=function(){return a},t(i)}catch(t){}return e}},function(t,n,e){var r=e(2),o=e(9),i=e(20),a=e(91),u=e(3).f;t.exports=function(t){var n=o.Symbol||(o.Symbol=i?{}:r.Symbol||{});"_"==t.charAt(0)||t in n||u(n,t,{value:a.f(t)})}},function(t,n,e){n.f=e(0)},function(t,n,e){var r=e(62),o=e(49).concat("length","prototype");n.f=Object.getOwnPropertyNames||function(t){return r(t,o)}},function(t,n){!function(n){"use strict";var e,r=Object.prototype,o=r.hasOwnProperty,i="function"==typeof Symbol?Symbol:{},a=i.iterator||"@@iterator",u=i.asyncIterator||"@@asyncIterator",c=i.toStringTag||"@@toStringTag",s="object"==typeof t,f=n.regeneratorRuntime;if(f)s&&(t.exports=f);else{(f=n.regeneratorRuntime=s?t.exports:{}).wrap=b;var l="suspendedStart",h="suspendedYield",p="executing",v="completed",y={},d={};d[a]=function(){return this};var m=Object.getPrototypeOf,g=m&&m(m(R([])));g&&g!==r&&o.call(g,a)&&(d=g);var x=k.prototype=C.prototype=Object.create(d);_.prototype=x.constructor=k,k.constructor=_,k[c]=_.displayName="GeneratorFunction",f.isGeneratorFunction=function(t){var n="function"==typeof t&&t.constructor;return!!n&&(n===_||"GeneratorFunction"===(n.displayName||n.name))},f.mark=function(t){return Object.setPrototypeOf?Object.setPrototypeOf(t,k):(t.__proto__=k,c in t||(t[c]="GeneratorFunction")),t.prototype=Object.create(x),t},f.awrap=function(t){return{__await:t}},S(j.prototype),j.prototype[u]=function(){return this},f.AsyncIterator=j,f.async=function(t,n,e,r){var o=new j(b(t,n,e,r));return f.isGeneratorFunction(n)?o:o.next().then(function(t){return t.done?t.value:o.next()})},S(x),x[c]="Generator",x[a]=function(){return this},x.toString=function(){return"[object Generator]"},f.keys=function(t){var n=[];for(var e in t)n.push(e);return n.reverse(),function e(){for(;n.length;){var r=n.pop();if(r in t)return e.value=r,e.done=!1,e}return e.done=!0,e}},f.values=R,E.prototype={constructor:E,reset:function(t){if(this.prev=0,this.next=0,this.sent=this._sent=e,this.done=!1,this.delegate=null,this.method="next",this.arg=e,this.tryEntries.forEach(P),!t)for(var n in this)"t"===n.charAt(0)&&o.call(this,n)&&!isNaN(+n.slice(1))&&(this[n]=e)},stop:function(){this.done=!0;var t=this.tryEntries[0].completion;if("throw"===t.type)throw t.arg;return this.rval},dispatchException:function(t){if(this.done)throw t;var n=this;function r(r,o){return u.type="throw",u.arg=t,n.next=r,o&&(n.method="next",n.arg=e),!!o}for(var i=this.tryEntries.length-1;i>=0;--i){var a=this.tryEntries[i],u=a.completion;if("root"===a.tryLoc)return r("end");if(a.tryLoc<=this.prev){var c=o.call(a,"catchLoc"),s=o.call(a,"finallyLoc");if(c&&s){if(this.prev<a.catchLoc)return r(a.catchLoc,!0);if(this.prev<a.finallyLoc)return r(a.finallyLoc)}else if(c){if(this.prev<a.catchLoc)return r(a.catchLoc,!0)}else{if(!s)throw new Error("try statement without catch or finally");if(this.prev<a.finallyLoc)return r(a.finallyLoc)}}}},abrupt:function(t,n){for(var e=this.tryEntries.length-1;e>=0;--e){var r=this.tryEntries[e];if(r.tryLoc<=this.prev&&o.call(r,"finallyLoc")&&this.prev<r.finallyLoc){var i=r;break}}i&&("break"===t||"continue"===t)&&i.tryLoc<=n&&n<=i.finallyLoc&&(i=null);var a=i?i.completion:{};return a.type=t,a.arg=n,i?(this.method="next",this.next=i.finallyLoc,y):this.complete(a)},complete:function(t,n){if("throw"===t.type)throw t.arg;return"break"===t.type||"continue"===t.type?this.next=t.arg:"return"===t.type?(this.rval=this.arg=t.arg,this.method="return",this.next="end"):"normal"===t.type&&n&&(this.next=n),y},finish:function(t){for(var n=this.tryEntries.length-1;n>=0;--n){var e=this.tryEntries[n];if(e.finallyLoc===t)return this.complete(e.completion,e.afterLoc),P(e),y}},catch:function(t){for(var n=this.tryEntries.length-1;n>=0;--n){var e=this.tryEntries[n];if(e.tryLoc===t){var r=e.completion;if("throw"===r.type){var o=r.arg;P(e)}return o}}throw new Error("illegal catch attempt")},delegateYield:function(t,n,r){return this.delegate={iterator:R(t),resultName:n,nextLoc:r},"next"===this.method&&(this.arg=e),y}}}function b(t,n,e,r){var o=n&&n.prototype instanceof C?n:C,i=Object.create(o.prototype),a=new E(r||[]);return i._invoke=function(t,n,e){var r=l;return function(o,i){if(r===p)throw new Error("Generator is already running");if(r===v){if("throw"===o)throw i;return T()}for(e.method=o,e.arg=i;;){var a=e.delegate;if(a){var u=O(a,e);if(u){if(u===y)continue;return u}}if("next"===e.method)e.sent=e._sent=e.arg;else if("throw"===e.method){if(r===l)throw r=v,e.arg;e.dispatchException(e.arg)}else"return"===e.method&&e.abrupt("return",e.arg);r=p;var c=w(t,n,e);if("normal"===c.type){if(r=e.done?v:h,c.arg===y)continue;return{value:c.arg,done:e.done}}"throw"===c.type&&(r=v,e.method="throw",e.arg=c.arg)}}}(t,e,a),i}function w(t,n,e){try{return{type:"normal",arg:t.call(n,e)}}catch(t){return{type:"throw",arg:t}}}function C(){}function _(){}function k(){}function S(t){["next","throw","return"].forEach(function(n){t[n]=function(t){return this._invoke(n,t)}})}function j(t){var n;this._invoke=function(e,r){function i(){return new Promise(function(n,i){!function n(e,r,i,a){var u=w(t[e],t,r);if("throw"!==u.type){var c=u.arg,s=c.value;return s&&"object"==typeof s&&o.call(s,"__await")?Promise.resolve(s.__await).then(function(t){n("next",t,i,a)},function(t){n("throw",t,i,a)}):Promise.resolve(s).then(function(t){c.value=t,i(c)},a)}a(u.arg)}(e,r,n,i)})}return n=n?n.then(i,i):i()}}function O(t,n){var r=t.iterator[n.method];if(r===e){if(n.delegate=null,"throw"===n.method){if(t.iterator.return&&(n.method="return",n.arg=e,O(t,n),"throw"===n.method))return y;n.method="throw",n.arg=new TypeError("The iterator does not provide a 'throw' method")}return y}var o=w(r,t.iterator,n.arg);if("throw"===o.type)return n.method="throw",n.arg=o.arg,n.delegate=null,y;var i=o.arg;return i?i.done?(n[t.resultName]=i.value,n.next=t.nextLoc,"return"!==n.method&&(n.method="next",n.arg=e),n.delegate=null,y):i:(n.method="throw",n.arg=new TypeError("iterator result is not an object"),n.delegate=null,y)}function A(t){var n={tryLoc:t[0]};1 in t&&(n.catchLoc=t[1]),2 in t&&(n.finallyLoc=t[2],n.afterLoc=t[3]),this.tryEntries.push(n)}function P(t){var n=t.completion||{};n.type="normal",delete n.arg,t.completion=n}function E(t){this.tryEntries=[{tryLoc:"root"}],t.forEach(A,this),this.reset(!0)}function R(t){if(t){var n=t[a];if(n)return n.call(t);if("function"==typeof t.next)return t;if(!isNaN(t.length)){var r=-1,i=function n(){for(;++r<t.length;)if(o.call(t,r))return n.value=t[r],n.done=!1,n;return n.value=e,n.done=!0,n};return i.next=i}}return{next:T}}function T(){return{value:e,done:!0}}}(function(){return this}()||Function("return this")())},function(t,n,e){var r=e(70),o=e(53),i=e(95),a=e(37);t.exports=function(t,n,e){if(!a(e))return!1;var u=typeof n;return!!("number"==u?o(e)&&i(n,e.length):"string"==u&&n in e)&&r(e[n],t)}},function(t,n){var e=9007199254740991,r=/^(?:0|[1-9]\d*)$/;t.exports=function(t,n){var o=typeof t;return!!(n=null==n?e:n)&&("number"==o||"symbol"!=o&&r.test(t))&&t>-1&&t%1==0&&t<n}},function(t,n,e){var r=e(198),o=1/0,i=1.7976931348623157e308;t.exports=function(t){return t?(t=r(t))===o||t===-o?(t<0?-1:1)*i:t==t?t:0:0===t?t:0}},function(t,n){t.exports=function(t){var n=-1,e=Array(t.size);return t.forEach(function(t){e[++n]=t}),e}},function(t,n){t.exports=function(t,n,e){return t==t&&(void 0!==e&&(t=t<=e?t:e),void 0!==n&&(t=t>=n?t:n)),t}},function(t,n,e){var r=e(55);t.exports=function(t,n){var e=-1,o=t.length,i=o-1;for(n=void 0===n?o:n;++e<n;){var a=r(e,i),u=t[a];t[a]=t[e],t[e]=u}return t.length=n,t}},function(t,n,e){var r=e(206),o=e(207);t.exports=function(t){return null==t?[]:r(t,o(t))}},function(t,n){t.exports=function(t,n){for(var e=-1,r=Array(t);++e<t;)r[e]=n(e);return r}},function(t,n){t.exports=function(t){return t.webpackPolyfill||(t.deprecate=function(){},t.paths=[],t.children||(t.children=[]),Object.defineProperty(t,"loaded",{enumerable:!0,get:function(){return t.l}}),Object.defineProperty(t,"id",{enumerable:!0,get:function(){return t.i}}),t.webpackPolyfill=1),t}},function(t,n,e){var r=e(96);t.exports=function(t){var n=r(t),e=n%1;return n==n?e?n-e:n:0}},function(t,n,e){var r=e(55);t.exports=function(t){var n=t.length;return n?t[r(0,n-1)]:void 0}},function(t,n,e){var r=e(117),o=e(75),i=e(77),a=e(78),u=i(function(t,n){return a(t)?r(t,o(n,1,a,!0)):[]});t.exports=u},function(t,n,e){var r=e(162),o=0;t.exports=function(t){var n=++o;return r(t)+n}},function(t,n,e){var r=e(203),o=e(205),i=e(25),a=e(94),u=e(103);t.exports=function(t,n,e){return n=(e?a(t,n,e):void 0===n)?1:u(n),(i(t)?r:o)(t,n)}},function(t,n,e){var r=e(101),o=e(219),i=e(103),a=9007199254740991,u=4294967295,c=Math.min;t.exports=function(t,n){if((t=i(t))<1||t>a)return[];var e=u,s=c(t,u);n=o(n),t-=u;for(var f=r(s,n);++e<t;)n(e);return f}},function(t,n){t.exports=function(t,n){return{value:n,done:!!t}}},function(t,n,e){"use strict";var r=e(61),o=e(18),i=e(30),a={};e(10)(a,e(0)("iterator"),function(){return this}),t.exports=function(t,n,e){t.prototype=r(a,{next:o(1,e)}),i(t,n+" Iterator")}},function(t,n,e){var r=e(3),o=e(4),i=e(16);t.exports=e(5)?Object.defineProperties:function(t,n){o(t);for(var e,a=i(n),u=a.length,c=0;u>c;)r.f(t,e=a[c++],n[e]);return t}},function(t,n,e){var r=e(12),o=e(17),i=e(48)("IE_PROTO"),a=Object.prototype;t.exports=Object.getPrototypeOf||function(t){return t=o(t),r(t,i)?t[i]:"function"==typeof t.constructor&&t instanceof t.constructor?t.constructor.prototype:t instanceof Object?a:null}},function(t,n,e){var r=e(17),o=e(16);e(114)("keys",function(){return function(t){return o(r(t))}})},function(t,n,e){var r=e(1),o=e(9),i=e(8);t.exports=function(t,n){var e=(o.Object||{})[t]||Object[t],a={};a[t]=n(e),r(r.S+r.F*i(function(){e(1)}),"Object",a)}},function(t,n,e){var r=e(116);t.exports=function(t,n){return new(r(t))(n)}},function(t,n,e){var r=e(6),o=e(50),i=e(0)("species");t.exports=function(t){var n;return o(t)&&("function"!=typeof(n=t.constructor)||n!==Array&&!o(n.prototype)||(n=void 0),r(n)&&null===(n=n[i])&&(n=void 0)),void 0===n?Array:n}},function(t,n,e){var r=e(67),o=e(71),i=e(72),a=e(51),u=e(73),c=e(74),s=200;t.exports=function(t,n,e,f){var l=-1,h=o,p=!0,v=t.length,y=[],d=n.length;if(!v)return y;e&&(n=a(n,u(e))),f?(h=i,p=!1):n.length>=s&&(h=c,p=!1,n=new r(n));t:for(;++l<v;){var m=t[l],g=null==e?m:e(m);if(m=f||0!==m?m:0,p&&g==g){for(var x=d;x--;)if(n[x]===g)continue t;y.push(m)}else h(n,g,f)||y.push(m)}return y}},function(t,n,e){var r=e(119),o=e(141),i=e(143),a=e(144),u=e(145);function c(t){var n=-1,e=null==t?0:t.length;for(this.clear();++n<e;){var r=t[n];this.set(r[0],r[1])}}c.prototype.clear=r,c.prototype.delete=o,c.prototype.get=i,c.prototype.has=a,c.prototype.set=u,t.exports=c},function(t,n,e){var r=e(120),o=e(134),i=e(140);t.exports=function(){this.size=0,this.__data__={hash:new r,map:new(i||o),string:new r}}},function(t,n,e){var r=e(121),o=e(130),i=e(131),a=e(132),u=e(133);function c(t){var n=-1,e=null==t?0:t.length;for(this.clear();++n<e;){var r=t[n];this.set(r[0],r[1])}}c.prototype.clear=r,c.prototype.delete=o,c.prototype.get=i,c.prototype.has=a,c.prototype.set=u,t.exports=c},function(t,n,e){var r=e(33);t.exports=function(){this.__data__=r?r(null):{},this.size=0}},function(t,n,e){var r=e(68),o=e(126),i=e(37),a=e(128),u=/^\[object .+?Constructor\]$/,c=Function.prototype,s=Object.prototype,f=c.toString,l=s.hasOwnProperty,h=RegExp("^"+f.call(l).replace(/[\\^$.*+?()[\]{}|]/g,"\\$&").replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g,"$1.*?")+"$");t.exports=function(t){return!(!i(t)||o(t))&&(r(t)?h:u).test(a(t))}},function(t,n){var e;e=function(){return this}();try{e=e||Function("return this")()||(0,eval)("this")}catch(t){"object"==typeof window&&(e=window)}t.exports=e},function(t,n,e){var r=e(36),o=Object.prototype,i=o.hasOwnProperty,a=o.toString,u=r?r.toStringTag:void 0;t.exports=function(t){var n=i.call(t,u),e=t[u];try{t[u]=void 0;var r=!0}catch(t){}var o=a.call(t);return r&&(n?t[u]=e:delete t[u]),o}},function(t,n){var e=Object.prototype.toString;t.exports=function(t){return e.call(t)}},function(t,n,e){var r=e(127),o=function(){var t=/[^.]+$/.exec(r&&r.keys&&r.keys.IE_PROTO||"");return t?"Symbol(src)_1."+t:""}();t.exports=function(t){return!!o&&o in t}},function(t,n,e){var r=e(23)["__core-js_shared__"];t.exports=r},function(t,n){var e=Function.prototype.toString;t.exports=function(t){if(null!=t){try{return e.call(t)}catch(t){}try{return t+""}catch(t){}}return""}},function(t,n){t.exports=function(t,n){return null==t?void 0:t[n]}},function(t,n){t.exports=function(t){var n=this.has(t)&&delete this.__data__[t];return this.size-=n?1:0,n}},function(t,n,e){var r=e(33),o="__lodash_hash_undefined__",i=Object.prototype.hasOwnProperty;t.exports=function(t){var n=this.__data__;if(r){var e=n[t];return e===o?void 0:e}return i.call(n,t)?n[t]:void 0}},function(t,n,e){var r=e(33),o=Object.prototype.hasOwnProperty;t.exports=function(t){var n=this.__data__;return r?void 0!==n[t]:o.call(n,t)}},function(t,n,e){var r=e(33),o="__lodash_hash_undefined__";t.exports=function(t,n){var e=this.__data__;return this.size+=this.has(t)?0:1,e[t]=r&&void 0===n?o:n,this}},function(t,n,e){var r=e(135),o=e(136),i=e(137),a=e(138),u=e(139);function c(t){var n=-1,e=null==t?0:t.length;for(this.clear();++n<e;){var r=t[n];this.set(r[0],r[1])}}c.prototype.clear=r,c.prototype.delete=o,c.prototype.get=i,c.prototype.has=a,c.prototype.set=u,t.exports=c},function(t,n){t.exports=function(){this.__data__=[],this.size=0}},function(t,n,e){var r=e(38),o=Array.prototype.splice;t.exports=function(t){var n=this.__data__,e=r(n,t);return!(e<0||(e==n.length-1?n.pop():o.call(n,e,1),--this.size,0))}},function(t,n,e){var r=e(38);t.exports=function(t){var n=this.__data__,e=r(n,t);return e<0?void 0:n[e][1]}},function(t,n,e){var r=e(38);t.exports=function(t){return r(this.__data__,t)>-1}},function(t,n,e){var r=e(38);t.exports=function(t,n){var e=this.__data__,o=r(e,t);return o<0?(++this.size,e.push([t,n])):e[o][1]=n,this}},function(t,n,e){var r=e(34)(e(23),"Map");t.exports=r},function(t,n,e){var r=e(39);t.exports=function(t){var n=r(this,t).delete(t);return this.size-=n?1:0,n}},function(t,n){t.exports=function(t){var n=typeof t;return"string"==n||"number"==n||"symbol"==n||"boolean"==n?"__proto__"!==t:null===t}},function(t,n,e){var r=e(39);t.exports=function(t){return r(this,t).get(t)}},function(t,n,e){var r=e(39);t.exports=function(t){return r(this,t).has(t)}},function(t,n,e){var r=e(39);t.exports=function(t,n){var e=r(this,t),o=e.size;return e.set(t,n),this.size+=e.size==o?0:1,this}},function(t,n){var e="__lodash_hash_undefined__";t.exports=function(t){return this.__data__.set(t,e),this}},function(t,n){t.exports=function(t){return this.__data__.has(t)}},function(t,n,e){var r=e(149),o=e(150),i=e(151);t.exports=function(t,n,e){return n==n?i(t,n,e):r(t,o,e)}},function(t,n){t.exports=function(t,n,e,r){for(var o=t.length,i=e+(r?1:-1);r?i--:++i<o;)if(n(t[i],i,t))return i;return-1}},function(t,n){t.exports=function(t){return t!=t}},function(t,n){t.exports=function(t,n,e){for(var r=e-1,o=t.length;++r<o;)if(t[r]===n)return r;return-1}},function(t,n){t.exports=function(t,n){for(var e=-1,r=n.length,o=t.length;++e<r;)t[o+e]=n[e];return t}},function(t,n,e){var r=e(36),o=e(76),i=e(25),a=r?r.isConcatSpreadable:void 0;t.exports=function(t){return i(t)||o(t)||!!(a&&t&&t[a])}},function(t,n,e){var r=e(35),o=e(24),i="[object Arguments]";t.exports=function(t){return o(t)&&r(t)==i}},function(t,n,e){var r=e(156),o=Math.max;t.exports=function(t,n,e){return n=o(void 0===n?t.length-1:n,0),function(){for(var i=arguments,a=-1,u=o(i.length-n,0),c=Array(u);++a<u;)c[a]=i[n+a];a=-1;for(var s=Array(n+1);++a<n;)s[a]=i[a];return s[n]=e(c),r(t,this,s)}}},function(t,n){t.exports=function(t,n,e){switch(e.length){case 0:return t.call(n);case 1:return t.call(n,e[0]);case 2:return t.call(n,e[0],e[1]);case 3:return t.call(n,e[0],e[1],e[2])}return t.apply(n,e)}},function(t,n,e){var r=e(158),o=e(161)(r);t.exports=o},function(t,n,e){var r=e(159),o=e(160),i=e(52),a=o?function(t,n){return o(t,"toString",{configurable:!0,enumerable:!1,value:r(n),writable:!0})}:i;t.exports=a},function(t,n){t.exports=function(t){return function(){return t}}},function(t,n,e){var r=e(34),o=function(){try{var t=r(Object,"defineProperty");return t({},"",{}),t}catch(t){}}();t.exports=o},function(t,n){var e=800,r=16,o=Date.now;t.exports=function(t){var n=0,i=0;return function(){var a=o(),u=r-(a-i);if(i=a,u>0){if(++n>=e)return arguments[0]}else n=0;return t.apply(void 0,arguments)}}},function(t,n,e){var r=e(163);t.exports=function(t){return null==t?"":r(t)}},function(t,n,e){var r=e(36),o=e(51),i=e(25),a=e(80),u=1/0,c=r?r.prototype:void 0,s=c?c.toString:void 0;t.exports=function t(n){if("string"==typeof n)return n;if(i(n))return o(n,t)+"";if(a(n))return s?s.call(n):"";var e=n+"";return"0"==e&&1/n==-u?"-0":e}},function(t,n,e){"use strict";e(165);var r=e(4),o=e(81),i=e(5),a=/./.toString,u=function(t){e(11)(RegExp.prototype,"toString",t,!0)};e(8)(function(){return"/a/b"!=a.call({source:"a",flags:"b"})})?u(function(){var t=r(this);return"/".concat(t.source,"/","flags"in t?t.flags:!i&&t instanceof RegExp?o.call(t):void 0)}):"toString"!=a.name&&u(function(){return a.call(this)})},function(t,n,e){e(5)&&"g"!=/./g.flags&&e(3).f(RegExp.prototype,"flags",{configurable:!0,get:e(81)})},function(t,n,e){var r=Date.prototype,o=r.toString,i=r.getTime;new Date(NaN)+""!="Invalid Date"&&e(11)(r,"toString",function(){var t=i.call(this);return t==t?o.call(this):"Invalid Date"})},function(t,n){t.exports=function(t,n,e,r){if(!(t instanceof n)||void 0!==r&&r in t)throw TypeError(e+": incorrect invocation!");return t}},function(t,n,e){var r=e(13),o=e(84),i=e(85),a=e(4),u=e(22),c=e(86),s={},f={};(n=t.exports=function(t,n,e,l,h){var p,v,y,d,m=h?function(){return t}:c(t),g=r(e,l,n?2:1),x=0;if("function"!=typeof m)throw TypeError(t+" is not iterable!");if(i(m)){for(p=u(t.length);p>x;x++)if((d=n?g(a(v=t[x])[0],v[1]):g(t[x]))===s||d===f)return d}else for(y=m.call(t);!(v=y.next()).done;)if((d=o(y,g,v.value,n))===s||d===f)return d}).BREAK=s,n.RETURN=f},function(t,n,e){var r=e(4),o=e(28),i=e(0)("species");t.exports=function(t,n){var e,a=r(t).constructor;return void 0===a||void 0==(e=r(a)[i])?n:o(e)}},function(t,n){t.exports=function(t,n,e){var r=void 0===e;switch(n.length){case 0:return r?t():t.call(e);case 1:return r?t(n[0]):t.call(e,n[0]);case 2:return r?t(n[0],n[1]):t.call(e,n[0],n[1]);case 3:return r?t(n[0],n[1],n[2]):t.call(e,n[0],n[1],n[2]);case 4:return r?t(n[0],n[1],n[2],n[3]):t.call(e,n[0],n[1],n[2],n[3])}return t.apply(e,n)}},function(t,n,e){var r=e(2),o=e(87).set,i=r.MutationObserver||r.WebKitMutationObserver,a=r.process,u=r.Promise,c="process"==e(15)(a);t.exports=function(){var t,n,e,s=function(){var r,o;for(c&&(r=a.domain)&&r.exit();t;){o=t.fn,t=t.next;try{o()}catch(r){throw t?e():n=void 0,r}}n=void 0,r&&r.enter()};if(c)e=function(){a.nextTick(s)};else if(!i||r.navigator&&r.navigator.standalone)if(u&&u.resolve){var f=u.resolve(void 0);e=function(){f.then(s)}}else e=function(){o.call(r,s)};else{var l=!0,h=document.createTextNode("");new i(s).observe(h,{characterData:!0}),e=function(){h.data=l=!l}}return function(r){var o={fn:r,next:void 0};n&&(n.next=o),t||(t=o,e()),n=o}}},function(t,n){t.exports=function(t){try{return{e:!1,v:t()}}catch(t){return{e:!0,v:t}}}},function(t,n,e){var r=e(2).navigator;t.exports=r&&r.userAgent||""},function(t,n,e){var r=e(4),o=e(6),i=e(88);t.exports=function(t,n){if(r(t),o(n)&&n.constructor===t)return n;var e=i.f(t);return(0,e.resolve)(n),e.promise}},function(t,n,e){var r=e(11);t.exports=function(t,n,e){for(var o in n)r(t,o,n[o],e);return t}},function(t,n,e){"use strict";var r=e(2),o=e(3),i=e(5),a=e(0)("species");t.exports=function(t){var n=r[t];i&&n&&!n[a]&&o.f(n,a,{configurable:!0,get:function(){return this}})}},function(t,n,e){var r=e(1);r(r.S+r.F,"Object",{assign:e(178)})},function(t,n,e){"use strict";var r=e(16),o=e(54),i=e(40),a=e(17),u=e(46),c=Object.assign;t.exports=!c||e(8)(function(){var t={},n={},e=Symbol(),r="abcdefghijklmnopqrst";return t[e]=7,r.split("").forEach(function(t){n[t]=t}),7!=c({},t)[e]||Object.keys(c({},n)).join("")!=r})?function(t,n){for(var e=a(t),c=arguments.length,s=1,f=o.f,l=i.f;c>s;)for(var h,p=u(arguments[s++]),v=f?r(p).concat(f(p)):r(p),y=v.length,d=0;y>d;)l.call(p,h=v[d++])&&(e[h]=p[h]);return e}:c},function(t,n,e){"use strict";var r=e(1),o=e(63)(!0);r(r.P,"Array",{includes:function(t){return o(this,t,arguments.length>1?arguments[1]:void 0)}}),e(44)("includes")},function(t,n,e){"use strict";var r=e(1),o=e(181);r(r.P+r.F*e(183)("includes"),"String",{includes:function(t){return!!~o(this,t,"includes").indexOf(t,arguments.length>1?arguments[1]:void 0)}})},function(t,n,e){var r=e(182),o=e(29);t.exports=function(t,n,e){if(r(n))throw TypeError("String#"+e+" doesn't accept regex!");return String(o(t))}},function(t,n,e){var r=e(6),o=e(15),i=e(0)("match");t.exports=function(t){var n;return r(t)&&(void 0!==(n=t[i])?!!n:"RegExp"==o(t))}},function(t,n,e){var r=e(0)("match");t.exports=function(t){var n=/./;try{"/./"[t](n)}catch(e){try{return n[r]=!1,!"/./"[t](n)}catch(t){}}return!0}},function(t,n,e){"use strict";var r=e(1),o=e(31)(2);r(r.P+r.F*!e(32)([].filter,!0),"Array",{filter:function(t){return o(this,t,arguments[1])}})},function(t,n,e){"use strict";var r=e(1),o=e(31)(3);r(r.P+r.F*!e(32)([].some,!0),"Array",{some:function(t){return o(this,t,arguments[1])}})},function(t,n,e){e(90)("asyncIterator")},function(t,n,e){"use strict";var r=e(2),o=e(12),i=e(5),a=e(1),u=e(11),c=e(188).KEY,s=e(8),f=e(45),l=e(30),h=e(19),p=e(0),v=e(91),y=e(90),d=e(189),m=e(50),g=e(4),x=e(6),b=e(14),w=e(43),C=e(18),_=e(61),k=e(190),S=e(191),j=e(3),O=e(16),A=S.f,P=j.f,E=k.f,R=r.Symbol,T=r.JSON,M=T&&T.stringify,z=p("_hidden"),N=p("toPrimitive"),F={}.propertyIsEnumerable,I=f("symbol-registry"),L=f("symbols"),D=f("op-symbols"),B=Object.prototype,Y="function"==typeof R,X=r.QObject,G=!X||!X.prototype||!X.prototype.findChild,V=i&&s(function(){return 7!=_(P({},"a",{get:function(){return P(this,"a",{value:7}).a}})).a})?function(t,n,e){var r=A(B,n);r&&delete B[n],P(t,n,e),r&&t!==B&&P(B,n,r)}:P,q=function(t){var n=L[t]=_(R.prototype);return n._k=t,n},H=Y&&"symbol"==typeof R.iterator?function(t){return"symbol"==typeof t}:function(t){return t instanceof R},W=function(t,n,e){return t===B&&W(D,n,e),g(t),n=w(n,!0),g(e),o(L,n)?(e.enumerable?(o(t,z)&&t[z][n]&&(t[z][n]=!1),e=_(e,{enumerable:C(0,!1)})):(o(t,z)||P(t,z,C(1,{})),t[z][n]=!0),V(t,n,e)):P(t,n,e)},$=function(t,n){g(t);for(var e,r=d(n=b(n)),o=0,i=r.length;i>o;)W(t,e=r[o++],n[e]);return t},U=function(t){var n=F.call(this,t=w(t,!0));return!(this===B&&o(L,t)&&!o(D,t))&&(!(n||!o(this,t)||!o(L,t)||o(this,z)&&this[z][t])||n)},K=function(t,n){if(t=b(t),n=w(n,!0),t!==B||!o(L,n)||o(D,n)){var e=A(t,n);return!e||!o(L,n)||o(t,z)&&t[z][n]||(e.enumerable=!0),e}},J=function(t){for(var n,e=E(b(t)),r=[],i=0;e.length>i;)o(L,n=e[i++])||n==z||n==c||r.push(n);return r},Q=function(t){for(var n,e=t===B,r=E(e?D:b(t)),i=[],a=0;r.length>a;)!o(L,n=r[a++])||e&&!o(B,n)||i.push(L[n]);return i};Y||(u((R=function(){if(this instanceof R)throw TypeError("Symbol is not a constructor!");var t=h(arguments.length>0?arguments[0]:void 0),n=function(e){this===B&&n.call(D,e),o(this,z)&&o(this[z],t)&&(this[z][t]=!1),V(this,t,C(1,e))};return i&&G&&V(B,t,{configurable:!0,set:n}),q(t)}).prototype,"toString",function(){return this._k}),S.f=K,j.f=W,e(92).f=k.f=J,e(40).f=U,e(54).f=Q,i&&!e(20)&&u(B,"propertyIsEnumerable",U,!0),v.f=function(t){return q(p(t))}),a(a.G+a.W+a.F*!Y,{Symbol:R});for(var Z="hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables".split(","),tt=0;Z.length>tt;)p(Z[tt++]);for(var nt=O(p.store),et=0;nt.length>et;)y(nt[et++]);a(a.S+a.F*!Y,"Symbol",{for:function(t){return o(I,t+="")?I[t]:I[t]=R(t)},keyFor:function(t){if(!H(t))throw TypeError(t+" is not a symbol!");for(var n in I)if(I[n]===t)return n},useSetter:function(){G=!0},useSimple:function(){G=!1}}),a(a.S+a.F*!Y,"Object",{create:function(t,n){return void 0===n?_(t):$(_(t),n)},defineProperty:W,defineProperties:$,getOwnPropertyDescriptor:K,getOwnPropertyNames:J,getOwnPropertySymbols:Q}),T&&a(a.S+a.F*(!Y||s(function(){var t=R();return"[null]"!=M([t])||"{}"!=M({a:t})||"{}"!=M(Object(t))})),"JSON",{stringify:function(t){for(var n,e,r=[t],o=1;arguments.length>o;)r.push(arguments[o++]);if(e=n=r[1],(x(n)||void 0!==t)&&!H(t))return m(n)||(n=function(t,n){if("function"==typeof e&&(n=e.call(this,t,n)),!H(n))return n}),r[1]=n,M.apply(T,r)}}),R.prototype[N]||e(10)(R.prototype,N,R.prototype.valueOf),l(R,"Symbol"),l(Math,"Math",!0),l(r.JSON,"JSON",!0)},function(t,n,e){var r=e(19)("meta"),o=e(6),i=e(12),a=e(3).f,u=0,c=Object.isExtensible||function(){return!0},s=!e(8)(function(){return c(Object.preventExtensions({}))}),f=function(t){a(t,r,{value:{i:"O"+ ++u,w:{}}})},l=t.exports={KEY:r,NEED:!1,fastKey:function(t,n){if(!o(t))return"symbol"==typeof t?t:("string"==typeof t?"S":"P")+t;if(!i(t,r)){if(!c(t))return"F";if(!n)return"E";f(t)}return t[r].i},getWeak:function(t,n){if(!i(t,r)){if(!c(t))return!0;if(!n)return!1;f(t)}return t[r].w},onFreeze:function(t){return s&&l.NEED&&c(t)&&!i(t,r)&&f(t),t}}},function(t,n,e){var r=e(16),o=e(54),i=e(40);t.exports=function(t){var n=r(t),e=o.f;if(e)for(var a,u=e(t),c=i.f,s=0;u.length>s;)c.call(t,a=u[s++])&&n.push(a);return n}},function(t,n,e){var r=e(14),o=e(92).f,i={}.toString,a="object"==typeof window&&window&&Object.getOwnPropertyNames?Object.getOwnPropertyNames(window):[];t.exports.f=function(t){return a&&"[object Window]"==i.call(t)?function(t){try{return o(t)}catch(t){return a.slice()}}(t):o(r(t))}},function(t,n,e){var r=e(40),o=e(18),i=e(14),a=e(43),u=e(12),c=e(57),s=Object.getOwnPropertyDescriptor;n.f=e(5)?s:function(t,n){if(t=i(t),n=a(n,!0),c)try{return s(t,n)}catch(t){}if(u(t,n))return o(!r.f.call(t,n),t[n])}},function(t,n,e){var r=e(1);r(r.S,"Array",{isArray:e(50)})},function(t,n,e){"use strict";var r=e(1),o=e(31)(0),i=e(32)([].forEach,!0);r(r.P+r.F*!i,"Array",{forEach:function(t){return o(this,t,arguments[1])}})},function(t,n,e){"use strict";var r=e(195)(!0);e(60)(String,"String",function(t){this._t=String(t),this._i=0},function(){var t,n=this._t,e=this._i;return e>=n.length?{value:void 0,done:!0}:(t=r(n,e),this._i+=t.length,{value:t,done:!1})})},function(t,n,e){var r=e(47),o=e(29);t.exports=function(t){return function(n,e){var i,a,u=String(o(n)),c=r(e),s=u.length;return c<0||c>=s?t?"":void 0:(i=u.charCodeAt(c))<55296||i>56319||c+1===s||(a=u.charCodeAt(c+1))<56320||a>57343?t?u.charAt(c):i:t?u.slice(c,c+2):a-56320+(i-55296<<10)+65536}}},function(t,n,e){"use strict";var r=e(13),o=e(1),i=e(17),a=e(84),u=e(85),c=e(22),s=e(197),f=e(86);o(o.S+o.F*!e(89)(function(t){Array.from(t)}),"Array",{from:function(t){var n,e,o,l,h=i(t),p="function"==typeof this?this:Array,v=arguments.length,y=v>1?arguments[1]:void 0,d=void 0!==y,m=0,g=f(h);if(d&&(y=r(y,v>2?arguments[2]:void 0,2)),void 0==g||p==Array&&u(g))for(e=new p(n=c(h.length));n>m;m++)s(e,m,d?y(h[m],m):h[m]);else for(l=g.call(h),e=new p;!(o=l.next()).done;m++)s(e,m,d?a(l,y,[o.value,m],!0):o.value);return e.length=m,e}})},function(t,n,e){"use strict";var r=e(3),o=e(18);t.exports=function(t,n,e){n in t?r.f(t,n,o(0,e)):t[n]=e}},function(t,n,e){var r=e(37),o=e(80),i=NaN,a=/^\s+|\s+$/g,u=/^[-+]0x[0-9a-f]+$/i,c=/^0b[01]+$/i,s=/^0o[0-7]+$/i,f=parseInt;t.exports=function(t){if("number"==typeof t)return t;if(o(t))return i;if(r(t)){var n="function"==typeof t.valueOf?t.valueOf():t;t=r(n)?n+"":n}if("string"!=typeof t)return 0===t?t:+t;t=t.replace(a,"");var e=c.test(t);return e||s.test(t)?f(t.slice(2),e?2:8):u.test(t)?i:+t}},function(t,n,e){var r=e(67),o=e(71),i=e(72),a=e(74),u=e(200),c=e(97),s=200;t.exports=function(t,n,e){var f=-1,l=o,h=t.length,p=!0,v=[],y=v;if(e)p=!1,l=i;else if(h>=s){var d=n?null:u(t);if(d)return c(d);p=!1,l=a,y=new r}else y=n?[]:v;t:for(;++f<h;){var m=t[f],g=n?n(m):m;if(m=e||0!==m?m:0,p&&g==g){for(var x=y.length;x--;)if(y[x]===g)continue t;n&&y.push(g),v.push(m)}else l(y,g,e)||(y!==v&&y.push(g),v.push(m))}return v}},function(t,n,e){var r=e(201),o=e(202),i=e(97),a=r&&1/i(new r([,-0]))[1]==1/0?function(t){return new r(t)}:o;t.exports=a},function(t,n,e){var r=e(34)(e(23),"Set");t.exports=r},function(t,n){t.exports=function(){}},function(t,n,e){var r=e(98),o=e(204),i=e(99);t.exports=function(t,n){return i(o(t),r(n,0,t.length))}},function(t,n){t.exports=function(t,n){var e=-1,r=t.length;for(n||(n=Array(r));++e<r;)n[e]=t[e];return n}},function(t,n,e){var r=e(98),o=e(99),i=e(100);t.exports=function(t,n){var e=i(t);return o(e,r(n,0,e.length))}},function(t,n,e){var r=e(51);t.exports=function(t,n){return r(n,function(n){return t[n]})}},function(t,n,e){var r=e(208),o=e(214),i=e(53);t.exports=function(t){return i(t)?r(t):o(t)}},function(t,n,e){var r=e(101),o=e(76),i=e(25),a=e(209),u=e(95),c=e(211),s=Object.prototype.hasOwnProperty;t.exports=function(t,n){var e=i(t),f=!e&&o(t),l=!e&&!f&&a(t),h=!e&&!f&&!l&&c(t),p=e||f||l||h,v=p?r(t.length,String):[],y=v.length;for(var d in t)!n&&!s.call(t,d)||p&&("length"==d||l&&("offset"==d||"parent"==d)||h&&("buffer"==d||"byteLength"==d||"byteOffset"==d)||u(d,y))||v.push(d);return v}},function(t,n,e){(function(t){var r=e(23),o=e(210),i=n&&!n.nodeType&&n,a=i&&"object"==typeof t&&t&&!t.nodeType&&t,u=a&&a.exports===i?r.Buffer:void 0,c=(u?u.isBuffer:void 0)||o;t.exports=c}).call(this,e(102)(t))},function(t,n){t.exports=function(){return!1}},function(t,n,e){var r=e(212),o=e(73),i=e(213),a=i&&i.isTypedArray,u=a?o(a):r;t.exports=u},function(t,n,e){var r=e(35),o=e(79),i=e(24),a={};a["[object Float32Array]"]=a["[object Float64Array]"]=a["[object Int8Array]"]=a["[object Int16Array]"]=a["[object Int32Array]"]=a["[object Uint8Array]"]=a["[object Uint8ClampedArray]"]=a["[object Uint16Array]"]=a["[object Uint32Array]"]=!0,a["[object Arguments]"]=a["[object Array]"]=a["[object ArrayBuffer]"]=a["[object Boolean]"]=a["[object DataView]"]=a["[object Date]"]=a["[object Error]"]=a["[object Function]"]=a["[object Map]"]=a["[object Number]"]=a["[object Object]"]=a["[object RegExp]"]=a["[object Set]"]=a["[object String]"]=a["[object WeakMap]"]=!1,t.exports=function(t){return i(t)&&o(t.length)&&!!a[r(t)]}},function(t,n,e){(function(t){var r=e(69),o=n&&!n.nodeType&&n,i=o&&"object"==typeof t&&t&&!t.nodeType&&t,a=i&&i.exports===o&&r.process,u=function(){try{var t=i&&i.require&&i.require("util").types;return t||a&&a.binding&&a.binding("util")}catch(t){}}();t.exports=u}).call(this,e(102)(t))},function(t,n,e){var r=e(215),o=e(216),i=Object.prototype.hasOwnProperty;t.exports=function(t){if(!r(t))return o(t);var n=[];for(var e in Object(t))i.call(t,e)&&"constructor"!=e&&n.push(e);return n}},function(t,n){var e=Object.prototype;t.exports=function(t){var n=t&&t.constructor;return t===("function"==typeof n&&n.prototype||e)}},function(t,n,e){var r=e(217)(Object.keys,Object);t.exports=r},function(t,n){t.exports=function(t,n){return function(e){return t(n(e))}}},function(t,n,e){var r=e(104),o=e(100);t.exports=function(t){return r(o(t))}},function(t,n,e){var r=e(52);t.exports=function(t){return"function"==typeof t?t:r}},function(t,n,e){var r=e(1);r(r.P,"Array",{fill:e(221)}),e(44)("fill")},function(t,n,e){"use strict";var r=e(17),o=e(64),i=e(22);t.exports=function(t){for(var n=r(this),e=i(n.length),a=arguments.length,u=o(a>1?arguments[1]:void 0,e),c=a>2?arguments[2]:void 0,s=void 0===c?e:o(c,e);s>u;)n[u++]=t;return n}},function(t,n,e){var r=e(1);r(r.S,"Date",{now:function(){return(new Date).getTime()}})},function(t,n,e){"use strict";e.r(n);var r=e(7),o=e.n(r),i=[{name:"up",vector:new o.a(0,-1)},{name:"right",vector:new o.a(1,0)},{name:"down",vector:new o.a(0,1)},{name:"left",vector:new o.a(-1,0)}],a={up:"down",right:"left",down:"up",left:"right"},u={up:"right",right:"down",down:"left",left:"up"},c={crosshair:[["up","right","down","left"]],tee:[["up","right","down"],["right","down","left"],["down","left","up"],["left","up","right"]],elbow:[["up","right"],["right","down"],["down","left"],["left","up"]],straight:[["up","down"],["right","left"]],half:[["up"],["right"],["down"],["left"]]},s=(e(27),e(58),e(59),e(113),e(66),e(105)),f=e.n(s),l=e(106),h=e.n(l);function p(t,n){for(var e=0;e<n.length;e++){var r=n[e];r.enumerable=r.enumerable||!1,r.configurable=!0,"value"in r&&(r.writable=!0),Object.defineProperty(t,r.key,r)}}var v=function(){function t(n){var e=n.x,r=n.y,i=n.type,a=void 0===i?null:i,u=n.connections,c=void 0===u?[]:u,s=n.connected,f=void 0!==s&&s;if(function(t,n){if(!(t instanceof n))throw new TypeError("Cannot call a class as a function")}(this,t),void 0===e||void 0===r)throw new Error("x and y required");this.id=h()("".concat(r,"-").concat(e,"--")),this.type=a,this.connections=c,this.connected=f||"provider"===a,this.actualized="provider"===a,this.vector=o()(e,r),this.randomized=!1}return function(t,n,e){n&&p(t.prototype,n),e&&p(t,e)}(t,[{key:"rotateNode",value:function(){if(0===this.connections.length)return new Error("attempt to rotate cell with no connections");this.connections=this.connections.map(function(t){return u[t]})}}],[{key:"getConnectionParamsByConnections",value:function(t){for(var n=Object.keys(c),e=0;e<n.length;e++)for(var r=n[e],o=c[r],i=0;i<o.length;i++)if(0===f()(o[i],t).length)return{position:i,type:r};return null}}]),t}(),y=(e(164),e(166),e(82),e(177),e(179),e(180),e(184),e(185),e(186),e(187),e(192),e(193),e(194),e(196),e(93),e(41)),d=e.n(y),m=e(56),g=e.n(m),x=e(107),b=e.n(x),w=e(26),C=e.n(w),_=e(108),k=e.n(_);function S(t){return function(t){if(Array.isArray(t)){for(var n=0,e=new Array(t.length);n<t.length;n++)e[n]=t[n];return e}}(t)||function(t){if(Symbol.iterator in Object(t)||"[object Arguments]"===Object.prototype.toString.call(t))return Array.from(t)}(t)||function(){throw new TypeError("Invalid attempt to spread non-iterable instance")}()}function j(t,n,e,r,o,i,a){try{var u=t[i](a),c=u.value}catch(t){return void e(t)}u.done?n(c):Promise.resolve(c).then(r,o)}function O(t){return function(){var n=this,e=arguments;return new Promise(function(r,o){var i=t.apply(n,e);function a(t){j(i,r,o,a,u,"next",t)}function u(t){j(i,r,o,a,u,"throw",t)}a(void 0)})}}function A(t,n){for(var e=0;e<n.length;e++){var r=n[e];r.enumerable=r.enumerable||!1,r.configurable=!0,"value"in r&&(r.writable=!0),Object.defineProperty(t,r.key,r)}}var P=function(){function t(n){var e=n.template,r=n.rows,o=n.columns,i=n.matrixChangeCallback;if(function(t,n){if(!(t instanceof n))throw new TypeError("Cannot call a class as a function")}(this,t),this.matrixChangeCallback=i,e){if(!t.validateTemplate(e))throw new Error("invalid template");this.matrix=t.createMatrixByTemplate(e)}else{if(!r||!o)throw new Error("impossible create matrix without params");this.matrix=t.createMatrixByRowsAndColumns(r,o)}}return function(t,n,e){n&&A(t.prototype,n),e&&A(t,e)}(t,[{key:"fillMatrix",value:function(){var n=O(regeneratorRuntime.mark(function n(e){return regeneratorRuntime.wrap(function(n){for(;;)switch(n.prev=n.next){case 0:return n.next=2,t.fillMatrix(this.matrix,e,this.matrixChangeCallback);case 2:case"end":return n.stop()}},n,this)}));return function(t){return n.apply(this,arguments)}}()},{key:"randomizeMatrix",value:function(){var n=O(regeneratorRuntime.mark(function n(e){return regeneratorRuntime.wrap(function(n){for(;;)switch(n.prev=n.next){case 0:return n.next=2,t.randomizeMatrix(this.matrix,this.matrixChangeCallback,e);case 2:case"end":return n.stop()}},n,this)}));return function(t){return n.apply(this,arguments)}}()},{key:"checkMatrixIsSolved",value:function(){return t.checkMatrixSolved(this.matrix)}},{key:"rotateNode",value:function(n){var e=t.getCellById(this.matrix,n);return!!e&&(e.rotateNode(),t.actualizeConnectionStatuses(this.matrix,this.matrixChangeCallback),!0)}},{key:"matrix",get:function(){return this._matrix},set:function(t){this._matrix=t,this.matrixChangeCallback&&this.matrixChangeCallback()}}],[{key:"createMatrixByRowsAndColumns",value:function(t,n){for(var e=[],r=function(t){e.push(Array.from({length:n},function(n,e){return new v({type:"simple",y:t,x:e})}))},o=0;o<t;o++)r(o);return e}},{key:"createMatrixByTemplate",value:function(t){var n=[];return t.forEach(function(t,e){n.push(t.map(function(t,n){var r;switch(t){case"provider":r="provider";break;case 1:r="simple";break;case 0:r="empty"}return new v({y:e,x:n,type:r})}))}),n}},{key:"validateTemplate",value:function(n){return Array.isArray(n)&&n.length>0&&function(t){var n=!0,e=!1,r=void 0;try{for(var o,i=t[Symbol.iterator]();!(n=(o=i.next()).done);n=!0){var a=o.value;if(!Array.isArray(a))return!1}}catch(t){e=!0,r=t}finally{try{n||null==i.return||i.return()}finally{if(e)throw r}}return!0}(n)&&function(t){var n=0,e=!0,r=!1,o=void 0;try{for(var i,a=t[Symbol.iterator]();!(e=(i=a.next()).done);e=!0){var u=i.value,c=!0,s=!1,f=void 0;try{for(var l,h=u[Symbol.iterator]();!(c=(l=h.next()).done);c=!0){var p=l.value;0!==p&&n++}}catch(t){s=!0,f=t}finally{try{c||null==h.return||h.return()}finally{if(s)throw f}}}}catch(t){r=!0,o=t}finally{try{e||null==a.return||a.return()}finally{if(r)throw o}}return n>1}(n)&&function(t){var n=t[0].length;return!t.slice(1).some(function(t){return t.length!==n})}(n)&&function(t){var n=0,e=!0,r=!1,o=void 0;try{for(var i,a=t[Symbol.iterator]();!(e=(i=a.next()).done);e=!0){var u=i.value,c=!0,s=!1,f=void 0;try{for(var l,h=u[Symbol.iterator]();!(c=(l=h.next()).done);c=!0){var p=l.value;if("provider"===p&&++n>1)return!1}}catch(t){s=!0,f=t}finally{try{c||null==h.return||h.return()}finally{if(s)throw f}}}}catch(t){r=!0,o=t}finally{try{e||null==a.return||a.return()}finally{if(r)throw o}}return!0}(n)&&function(n){var e=n.map(function(t,n){return t.map(function(t,e){return{vector:new o.a(e,n),type:t?null:"empty",connectable:!1}})}),r=t.getAllNotEmptyCells(e),i=r[d()(0,r.length-1)];return function n(e,r){var o=t.getNotEmptyCellNeighbors(e,r);o&&(!0!==r.connectable&&(r.connectable=!0),o.filter(function(t){return!t.connectable}).forEach(function(t){return n(e,t)}))}(e,i),!e.some(function(t){return t.some(function(t){return"empty"!==t.type&&!t.connectable})})}(n)}},{key:"getAllNotEmptyCells",value:function(t){var n=[],e=!0,r=!1,o=void 0;try{for(var i,a=t[Symbol.iterator]();!(e=(i=a.next()).done);e=!0){var u=i.value,c=!0,s=!1,f=void 0;try{for(var l,h=u[Symbol.iterator]();!(c=(l=h.next()).done);c=!0){var p=l.value;"empty"!==p.type&&n.push(p)}}catch(t){s=!0,f=t}finally{try{c||null==h.return||h.return()}finally{if(s)throw f}}}}catch(t){r=!0,o=t}finally{try{e||null==a.return||a.return()}finally{if(r)throw o}}return n}},{key:"getCellByType",value:function(t,n){var e=!0,r=!1,o=void 0;try{for(var i,a=t[Symbol.iterator]();!(e=(i=a.next()).done);e=!0){var u=i.value,c=!0,s=!1,f=void 0;try{for(var l,h=u[Symbol.iterator]();!(c=(l=h.next()).done);c=!0){var p=l.value;if(p.type===n)return p}}catch(t){s=!0,f=t}finally{try{c||null==h.return||h.return()}finally{if(s)throw f}}}}catch(t){r=!0,o=t}finally{try{e||null==a.return||a.return()}finally{if(r)throw o}}return null}},{key:"getCellById",value:function(t,n){var e=!0,r=!1,o=void 0;try{for(var i,a=t[Symbol.iterator]();!(e=(i=a.next()).done);e=!0){var u=i.value,c=!0,s=!1,f=void 0;try{for(var l,h=u[Symbol.iterator]();!(c=(l=h.next()).done);c=!0){var p=l.value;if(p.id===n)return p}}catch(t){s=!0,f=t}finally{try{c||null==h.return||h.return()}finally{if(s)throw f}}}}catch(t){r=!0,o=t}finally{try{e||null==a.return||a.return()}finally{if(r)throw o}}return null}},{key:"getCellByVector",value:function(n,e){return t.checkVectorIsInMatrix(n,e)?n[e.y][e.x]:null}},{key:"getCellNeighbors",value:function(n,e){var r=[],o=!0,a=!1,u=void 0;try{for(var c,s=i[Symbol.iterator]();!(o=(c=s.next()).done);o=!0){var f=c.value,l=e.vector.clone().add(f.vector),h=t.getCellByVector(n,l);h&&r.push(h)}}catch(t){a=!0,u=t}finally{try{o||null==s.return||s.return()}finally{if(a)throw u}}return r}},{key:"getNotEmptyCellNeighbors",value:function(n,e){return t.getCellNeighbors(n,e).filter(function(t){return"empty"!==t.type})}},{key:"getCellNeighborsReadyToConnect",value:function(n,e){var r=t.getCellNeighbors(n,e);if(r.length>0){var o=r.filter(function(n){return t.cellIsReadyToConnect(n)});if(o.length>0)return o}return null}},{key:"getConsumerCellsWithReadyToConnectNeighbor",value:function(n){return t.getAllCellsByType(n,"consumer").filter(function(e){return t.cellHasNeighborsReadyToConnect(n,e)})}},{key:"getDirectionByNeighborsVectors",value:function(t,n){var e=t.clone().subtract(n),r=e.x,o=e.y;if(0===r)switch(o){case 1:return"up";case-1:return"down"}if(0===o)switch(r){case 1:return"left";case-1:return"right"}}},{key:"getAllCellsByType",value:function(t,n){var e=[];return t.forEach(function(t){var r=t.filter(function(t){if(Array.isArray(n)){var e=!0,r=!1,o=void 0;try{for(var i,a=n[Symbol.iterator]();!(e=(i=a.next()).done);e=!0){var u=i.value;if(t.type===u)return!0}}catch(t){r=!0,o=t}finally{try{e||null==a.return||a.return()}finally{if(r)throw o}}return!1}return t.type===n});e=S(e).concat(S(r))}),e.length>0?e:null}},{key:"checkVectorIsInMatrix",value:function(t,n){var e=n.x,r=n.y,o=t.length-1,i=t[0].length-1;return e>=0&&e<=i&&r>=0&&r<=o}},{key:"cellHasNeighborsReadyToConnect",value:function(n,e){return t.getNotEmptyCellNeighbors(n,e).some(function(n){return t.cellIsReadyToConnect(n)})}},{key:"cellIsReadyToConnect",value:function(t){var n=t.type,e=t.connections;return"simple"===n&&0===e.length}},{key:"checkCellsAreConnected",value:function(n,e){var r=t.getDirectionByNeighborsVectors(n.vector,e.vector),o=a[r];return n.connections.includes(r)&&e.connections.includes(o)}},{key:"checkMatrixSolved",value:function(n){return!t.getAllCellsByType(n,["consumer","provider"]).some(function(t){return!1===t.connected})}},{key:"connectCells",value:function(n,e){var r=t.getDirectionByNeighborsVectors(n.vector,e.vector),o=a[r];n.connections=g()(n.connections,[r]),e.connections=g()(e.connections,[o])}},{key:"createProvider",value:function(n,e){var r=t.getAllNotEmptyCells(n),o=r[d()(0,r.length-1)];return Object.assign(o,{type:"provider",connected:!0,actualized:!0}),e&&e(),o}},{key:"asyncCreateInitialConsumer",value:function(n,e,r){return new Promise(function(o){setTimeout(function(){var r=t.getCellNeighborsReadyToConnect(n,e);if(r){var i=C()(r);i.type="consumer",i.connected=!0,t.connectCells(e,i)}o()},r)})}},{key:"createInitialConsumer",value:function(n,e){var r=t.getCellNeighborsReadyToConnect(n,e);if(r){var o=C()(r);o.type="consumer",o.connected=!0,t.connectCells(e,o)}}},{key:"fillCellNeighbors",value:function(n,e){var r=t.getCellNeighborsReadyToConnect(n,e);if(r){e.type="simple";var o=b()(r,2),i=!0,a=!1,u=void 0;try{for(var c,s=o[Symbol.iterator]();!(i=(c=s.next()).done);i=!0){var f=c.value;f.type="consumer",t.connectCells(e,f)}}catch(t){a=!0,u=t}finally{try{i||null==s.return||s.return()}finally{if(a)throw u}}return o}return null}},{key:"recursiveFillConsumerCellsNeighbors",value:function(n){var e=t.getConsumerCellsWithReadyToConnectNeighbor(n);e.length>0&&(t.fillCellNeighbors(n,C()(e)),t.recursiveFillConsumerCellsNeighbors(n))}},{key:"asyncRecursiveFillConsumerCellsNeighbors",value:function(n,e,r){return new Promise(function(o){setTimeout(O(regeneratorRuntime.mark(function i(){var a;return regeneratorRuntime.wrap(function(i){for(;;)switch(i.prev=i.next){case 0:if((a=t.getConsumerCellsWithReadyToConnectNeighbor(n)).length>0&&(t.fillCellNeighbors(n,C()(a)),t.actualizeConnectionStatuses(n,r)),!(t.getConsumerCellsWithReadyToConnectNeighbor(n).length>0)){i.next=5;break}return i.next=5,t.asyncRecursiveFillConsumerCellsNeighbors(n,e,r);case 5:o();case 6:case"end":return i.stop()}},i,this)})),e)})}},{key:"asyncCreateInitialConsumerAndRecursivelyFillHisNeighbors",value:function(){var n=O(regeneratorRuntime.mark(function n(e,r,o,i){return regeneratorRuntime.wrap(function(n){for(;;)switch(n.prev=n.next){case 0:return n.next=2,t.asyncCreateInitialConsumer(e,r,o);case 2:return n.next=4,t.asyncRecursiveFillConsumerCellsNeighbors(e,o,i);case 4:case"end":return n.stop()}},n,this)}));return function(t,e,r,o){return n.apply(this,arguments)}}()},{key:"createInitialConsumerAndRecursivelyFillHisNeighbors",value:function(n,e){t.createInitialConsumer(n,e),t.recursiveFillConsumerCellsNeighbors(n)}},{key:"fillMatrix",value:function(){var n=O(regeneratorRuntime.mark(function n(e,r,o){var i;return regeneratorRuntime.wrap(function(n){for(;;)switch(n.prev=n.next){case 0:i=t.getCellByType(e,"provider")||t.createProvider(e,o);case 1:if(!t.cellHasNeighborsReadyToConnect(e,i)){n.next=11;break}if(!r){n.next=7;break}return n.next=5,t.asyncCreateInitialConsumerAndRecursivelyFillHisNeighbors(e,i,r,o);case 5:n.next=9;break;case 7:t.createInitialConsumerAndRecursivelyFillHisNeighbors(e,i),t.actualizeConnectionStatuses(e,o);case 9:n.next=1;break;case 11:case"end":return n.stop()}},n,this)}));return function(t,e,r){return n.apply(this,arguments)}}()},{key:"markConnectedCellsRecursively",value:function(n,e){var r=t.getNotEmptyCellNeighbors(n,e).filter(function(n){return!n.actualized&&t.checkCellsAreConnected(n,e)}),o=!0,i=!1,a=void 0;try{for(var u,c=r[Symbol.iterator]();!(o=(u=c.next()).done);o=!0){var s=u.value;Object.assign(s,{connected:!0,actualized:!0}),t.markConnectedCellsRecursively(n,s)}}catch(t){i=!0,a=t}finally{try{o||null==c.return||c.return()}finally{if(i)throw a}}}},{key:"actualizeConnectionStatuses",value:function(n,e){var r=t.getAllNotEmptyCells(n),o=!0,i=!1,a=void 0;try{for(var u,c=r[Symbol.iterator]();!(o=(u=c.next()).done);o=!0){var s=u.value;"provider"!==s.type&&(s.actualized=!1)}}catch(t){i=!0,a=t}finally{try{o||null==c.return||c.return()}finally{if(i)throw a}}t.markConnectedCellsRecursively(n,t.getCellByType(n,"provider"));var f=!0,l=!1,h=void 0;try{for(var p,v=r[Symbol.iterator]();!(f=(p=v.next()).done);f=!0){var y=p.value;y.actualized||Object.assign(y,{actualized:!0,connected:!1})}}catch(t){l=!0,h=t}finally{try{f||null==v.return||v.return()}finally{if(l)throw h}}e&&e()}},{key:"randomizeCell",value:function(t){k()(d()(1,3),function(){return t.rotateNode()}),Object.assign(t,{randomized:!1})}},{key:"asyncRandomizeRow",value:function(n,e){return new Promise(function(r){setTimeout(function(){var e=!0,o=!1,i=void 0;try{for(var a,u=n[Symbol.iterator]();!(e=(a=u.next()).done);e=!0){var c=a.value;"empty"!==c.type&&t.randomizeCell(c)}}catch(t){o=!0,i=t}finally{try{e||null==u.return||u.return()}finally{if(o)throw i}}r()},e)})}},{key:"randomizeMatrix",value:function(){var n=O(regeneratorRuntime.mark(function n(e,r,o){var i,a,u,c,s,f;return regeneratorRuntime.wrap(function(n){for(;;)switch(n.prev=n.next){case 0:if(!o){n.next=30;break}i=!0,a=!1,u=void 0,n.prev=4,c=e[Symbol.iterator]();case 6:if(i=(s=c.next()).done){n.next=14;break}return f=s.value,n.next=10,t.asyncRandomizeRow(f,o);case 10:t.actualizeConnectionStatuses(e,r);case 11:i=!0,n.next=6;break;case 14:n.next=20;break;case 16:n.prev=16,n.t0=n.catch(4),a=!0,u=n.t0;case 20:n.prev=20,n.prev=21,i||null==c.return||c.return();case 23:if(n.prev=23,!a){n.next=26;break}throw u;case 26:return n.finish(23);case 27:return n.finish(20);case 28:n.next=33;break;case 30:t.getAllNotEmptyCells(e).forEach(function(n){t.randomizeCell(n)}),t.actualizeConnectionStatuses(e,r);case 33:case"end":return n.stop()}},n,this,[[4,16,20,28],[21,,23,27]])}));return function(t,e,r){return n.apply(this,arguments)}}()}]),t}();e(220),e(222);function E(t,n){for(var e=0;e<n.length;e++){var r=n[e];r.enumerable=r.enumerable||!1,r.configurable=!0,"value"in r&&(r.writable=!0),Object.defineProperty(t,r.key,r)}}var R=function(){function t(n){var e=n.updateInterval,r=void 0===e?1e3:e,o=n.timeChangeCallback,i=n.statusChangeCallback;!function(t,n){if(!(t instanceof n))throw new TypeError("Cannot call a class as a function")}(this,t),this.timeChangeCallback=o,this.statusChangeCallback=i,this._updateInterval=r,this.status="reset",this.time=0,this._interval=null,this._offset=null}return function(t,n,e){n&&E(t.prototype,n),e&&E(t,e)}(t,[{key:"update",value:function(){var t=Date.now();this.time+=t-this._offset,this._offset=t}},{key:"start",value:function(){var t=this;"on"!==this.status&&(this._offset=Date.now(),this._interval=setInterval(function(){t.update()},this._updateInterval),this.status="on")}},{key:"pause",value:function(t){var n=t.silent,e=void 0!==n&&n;clearInterval(this._interval),this._interval=null,e||(this.status="pause")}},{key:"reset",value:function(){"on"===this.status&&this.pause({silent:!0}),this.time=0,this.status="reset"}},{key:"status",get:function(){return this._status},set:function(t){this._status=t,this.statusChangeCallback&&this.statusChangeCallback(this.status)}},{key:"time",get:function(){return this._time},set:function(t){this._time=t,this.timeChangeCallback&&this.timeChangeCallback(this.time)}}]),t}();function T(t,n,e,r,o,i,a){try{var u=t[i](a),c=u.value}catch(t){return void e(t)}u.done?n(c):Promise.resolve(c).then(r,o)}function M(t){return function(){var n=this,e=arguments;return new Promise(function(r,o){var i=t.apply(n,e);function a(t){T(i,r,o,a,u,"next",t)}function u(t){T(i,r,o,a,u,"throw",t)}a(void 0)})}}function z(t,n){for(var e=0;e<n.length;e++){var r=n[e];r.enumerable=r.enumerable||!1,r.configurable=!0,"value"in r&&(r.writable=!0),Object.defineProperty(t,r.key,r)}}var N=function(){function t(n){var e=this,r=n.template,o=void 0===r?null:r,i=n.rows,a=void 0===i?null:i,u=n.columns,c=void 0===u?null:u,s=n.matrixChangeCallback,f=void 0===s?null:s,l=n.stopwatchChangeCallback,h=void 0===l?null:l,p=n.movesChangeCallback,v=void 0===p?null:p,y=n.startedChangeCallback,d=void 0===y?null:y,m=n.finishedChangeCallback,g=void 0===m?null:m,x=n.readyChangeCallback,b=void 0===x?null:x,w=n.matrixFillingChangeCallback,C=void 0===w?null:w,_=n.matrixFilledChangeCallback,k=void 0===_?null:_,S=n.matrixRandomizingChangeCallback,j=void 0===S?null:S,O=n.matrixRandomizedChangeCallback,A=void 0===O?null:O,E=n.fillAnimationDelay,T=void 0===E?0:E,M=n.randomizeAnimationDelay,z=void 0===M?0:M,N=n.fill,F=void 0===N||N,I=n.randomize,L=void 0===I||I;!function(t,n){if(!(t instanceof n))throw new TypeError("Cannot call a class as a function")}(this,t),this.stopwatchInstance=new R({timeChangeCallback:function(t){e.stopwatch=t}}),this.template=o,this.rows=a,this.columns=c,this.fillAnimationDelay=T,this.randomizeAnimationDelay=z,this.matrixChangeCallback=f,this.stopwatchChangeCallback=h,this.movesChangeCallback=v,this.startedChangeCallback=d,this.finishedChangeCallback=g,this.readyChangeCallback=b,this.matrixRandomizedChangeCallback=A,this.matrixFilledChangeCallback=k,this.matrixFillingChangeCallback=C,this.matrixRandomizingChangeCallback=j,this.setInitialStatuses(),this.matrixInstance=new P({template:this.template,rows:this.rows,columns:this.columns,matrixChangeCallback:this.matrixChangeCallbackMiddleware(this.matrixChangeCallback)}),this.prepare({fill:F,randomize:L})}return function(t,n,e){n&&z(t.prototype,n),e&&z(t,e)}(t,[{key:"setInitialStatuses",value:function(){this.moves=0,this.ready=!1,this.started=!1,this.finished=!1,this.matrixFilling=!1,this.matrixFilled=!1,this.matrixRandomizing=!1,this.matrixRandomized=!1}},{key:"prepare",value:function(){var t=M(regeneratorRuntime.mark(function t(n){var e,r;return regeneratorRuntime.wrap(function(t){for(;;)switch(t.prev=t.next){case 0:if(e=n.fill,r=n.randomize,!e){t.next=7;break}return t.next=4,this.fillMatrix();case 4:if(!r){t.next=7;break}return t.next=7,this.randomizeMatrix();case 7:case"end":return t.stop()}},t,this)}));return function(n){return t.apply(this,arguments)}}()},{key:"fillMatrix",value:function(){var t=M(regeneratorRuntime.mark(function t(){return regeneratorRuntime.wrap(function(t){for(;;)switch(t.prev=t.next){case 0:if(this.matrixFilled){t.next=6;break}return this.matrixFilling=!0,t.next=4,this.matrixInstance.fillMatrix(this.fillAnimationDelay);case 4:this.matrixFilling=!1,this.matrixFilled=!0;case 6:case"end":return t.stop()}},t,this)}));return function(){return t.apply(this,arguments)}}()},{key:"rotateNode",value:function(t){this.ready&&!this.finished&&this.moves++,this.matrixInstance.rotateNode(t)}},{key:"randomizeMatrix",value:function(){var t=M(regeneratorRuntime.mark(function t(){return regeneratorRuntime.wrap(function(t){for(;;)switch(t.prev=t.next){case 0:if(this.matrixRandomized){t.next=6;break}return this.matrixRandomizing=!0,t.next=4,this.matrixInstance.randomizeMatrix(this.randomizeAnimationDelay);case 4:this.matrixRandomizing=!1,this.matrixRandomized=!0;case 6:case"end":return t.stop()}},t,this)}));return function(){return t.apply(this,arguments)}}()},{key:"destroy",value:function(){this.stopwatchInstance&&this.stopwatchInstance.reset()}},{key:"matrixChangeCallbackMiddleware",value:function(t){var n=this;return function(){n.started&&n.matrixInstance.checkMatrixIsSolved()&&(n.finished=!0),t&&t()}}},{key:"actualizeReady",value:function(){this.matrixFilled&&this.matrixRandomized&&(this.ready=!0)}},{key:"matrix",get:function(){return this._matrixInstance.matrix},set:function(t){this._matrixInstance.matrix=t}},{key:"matrixInstance",get:function(){return this._matrixInstance},set:function(t){this._matrixInstance=t}},{key:"stopwatch",get:function(){return this._stopwatch},set:function(t){this._stopwatch=t,this.stopwatchChangeCallback&&this.stopwatchChangeCallback(t)}},{key:"moves",get:function(){return this._moves},set:function(t){this._moves=t,!this.started&&this.ready&&(this.started=!0),this.movesChangeCallback&&this.movesChangeCallback(t)}},{key:"started",get:function(){return this._started},set:function(t){this._started=t,!0===t&&this.stopwatchInstance&&"on"!==this.stopwatchInstance.status&&this.stopwatchInstance.start(),this.startedChangeCallback&&this.startedChangeCallback(t)}},{key:"ready",get:function(){return this._ready},set:function(t){this._ready=t,this.readyChangeCallback&&this.readyChangeCallback(t)}},{key:"finished",get:function(){return this._finished},set:function(t){this._finished=t,this.stopwatchInstance.pause({}),this.finishedChangeCallback&&this.finishedChangeCallback(t)}},{key:"matrixFilling",get:function(){return this._matrixFilling},set:function(t){this._matrixFilling=t,this.matrixFillingChangeCallback&&this.matrixFillingChangeCallback(t)}},{key:"matrixFilled",get:function(){return this._matrixRandomized},set:function(t){this._matrixFilled=t,this.matrixFilledChangeCallback&&this.matrixFilledChangeCallback(t),this.actualizeReady()}},{key:"matrixRandomizing",get:function(){return this._matrixRandomizing},set:function(t){this._matrixRandomizing=t,this.matrixRandomizingChangeCallback&&this.matrixRandomizingChangeCallback(t)}},{key:"matrixRandomized",get:function(){return this._matrixRandomized},set:function(t){this._matrixRandomized=t,this.matrixRandomizedChangeCallback&&this.matrixRandomizedChangeCallback(t),this.actualizeReady()}}]),t}();e.d(n,"directions",function(){return i}),e.d(n,"oppositeConnections",function(){return a}),e.d(n,"nextConnections",function(){return u}),e.d(n,"connectionTypes",function(){return c}),e.d(n,"Cell",function(){return v}),e.d(n,"Matrix",function(){return P}),e.d(n,"Netwalk",function(){return N})}])});

/***/ }),

/***/ "9e69":
/***/ (function(module, exports, __webpack_require__) {

var root = __webpack_require__("2b3e");

/** Built-in value references. */
var Symbol = root.Symbol;

module.exports = Symbol;


/***/ }),

/***/ "a68e":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "\n.PlayingField_row_3QSjk{display:-webkit-box;display:-ms-flexbox;display:flex\n}", ""]);

// exports
exports.locals = {
	"row": "PlayingField_row_3QSjk"
};

/***/ }),

/***/ "abb4":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/* global console: false */

/**
 * Reporter that handles the reporting of logs, warnings and errors.
 * @public
 * @param {boolean} quiet Tells if the reporter should be quiet or not.
 */
module.exports = function(quiet) {
    function noop() {
        //Does nothing.
    }

    var reporter = {
        log: noop,
        warn: noop,
        error: noop
    };

    if(!quiet && window.console) {
        var attachFunction = function(reporter, name) {
            //The proxy is needed to be able to call the method with the console context,
            //since we cannot use bind.
            reporter[name] = function reporterProxy() {
                var f = console[name];
                if (f.apply) { //IE9 does not support console.log.apply :)
                    f.apply(console, arguments);
                } else {
                    for (var i = 0; i < arguments.length; i++) {
                        f(arguments[i]);
                    }
                }
            };
        };

        attachFunction(reporter, "log");
        attachFunction(reporter, "warn");
        attachFunction(reporter, "error");
    }

    return reporter;
};

/***/ }),

/***/ "b047":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("1a8c"),
    now = __webpack_require__("408c"),
    toNumber = __webpack_require__("b4b0");

/** Error message constants. */
var FUNC_ERROR_TEXT = 'Expected a function';

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeMax = Math.max,
    nativeMin = Math.min;

/**
 * Creates a debounced function that delays invoking `func` until after `wait`
 * milliseconds have elapsed since the last time the debounced function was
 * invoked. The debounced function comes with a `cancel` method to cancel
 * delayed `func` invocations and a `flush` method to immediately invoke them.
 * Provide `options` to indicate whether `func` should be invoked on the
 * leading and/or trailing edge of the `wait` timeout. The `func` is invoked
 * with the last arguments provided to the debounced function. Subsequent
 * calls to the debounced function return the result of the last `func`
 * invocation.
 *
 * **Note:** If `leading` and `trailing` options are `true`, `func` is
 * invoked on the trailing edge of the timeout only if the debounced function
 * is invoked more than once during the `wait` timeout.
 *
 * If `wait` is `0` and `leading` is `false`, `func` invocation is deferred
 * until to the next tick, similar to `setTimeout` with a timeout of `0`.
 *
 * See [David Corbacho's article](https://css-tricks.com/debouncing-throttling-explained-examples/)
 * for details over the differences between `_.debounce` and `_.throttle`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Function
 * @param {Function} func The function to debounce.
 * @param {number} [wait=0] The number of milliseconds to delay.
 * @param {Object} [options={}] The options object.
 * @param {boolean} [options.leading=false]
 *  Specify invoking on the leading edge of the timeout.
 * @param {number} [options.maxWait]
 *  The maximum time `func` is allowed to be delayed before it's invoked.
 * @param {boolean} [options.trailing=true]
 *  Specify invoking on the trailing edge of the timeout.
 * @returns {Function} Returns the new debounced function.
 * @example
 *
 * // Avoid costly calculations while the window size is in flux.
 * jQuery(window).on('resize', _.debounce(calculateLayout, 150));
 *
 * // Invoke `sendMail` when clicked, debouncing subsequent calls.
 * jQuery(element).on('click', _.debounce(sendMail, 300, {
 *   'leading': true,
 *   'trailing': false
 * }));
 *
 * // Ensure `batchLog` is invoked once after 1 second of debounced calls.
 * var debounced = _.debounce(batchLog, 250, { 'maxWait': 1000 });
 * var source = new EventSource('/stream');
 * jQuery(source).on('message', debounced);
 *
 * // Cancel the trailing debounced invocation.
 * jQuery(window).on('popstate', debounced.cancel);
 */
function debounce(func, wait, options) {
  var lastArgs,
      lastThis,
      maxWait,
      result,
      timerId,
      lastCallTime,
      lastInvokeTime = 0,
      leading = false,
      maxing = false,
      trailing = true;

  if (typeof func != 'function') {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  wait = toNumber(wait) || 0;
  if (isObject(options)) {
    leading = !!options.leading;
    maxing = 'maxWait' in options;
    maxWait = maxing ? nativeMax(toNumber(options.maxWait) || 0, wait) : maxWait;
    trailing = 'trailing' in options ? !!options.trailing : trailing;
  }

  function invokeFunc(time) {
    var args = lastArgs,
        thisArg = lastThis;

    lastArgs = lastThis = undefined;
    lastInvokeTime = time;
    result = func.apply(thisArg, args);
    return result;
  }

  function leadingEdge(time) {
    // Reset any `maxWait` timer.
    lastInvokeTime = time;
    // Start the timer for the trailing edge.
    timerId = setTimeout(timerExpired, wait);
    // Invoke the leading edge.
    return leading ? invokeFunc(time) : result;
  }

  function remainingWait(time) {
    var timeSinceLastCall = time - lastCallTime,
        timeSinceLastInvoke = time - lastInvokeTime,
        timeWaiting = wait - timeSinceLastCall;

    return maxing
      ? nativeMin(timeWaiting, maxWait - timeSinceLastInvoke)
      : timeWaiting;
  }

  function shouldInvoke(time) {
    var timeSinceLastCall = time - lastCallTime,
        timeSinceLastInvoke = time - lastInvokeTime;

    // Either this is the first call, activity has stopped and we're at the
    // trailing edge, the system time has gone backwards and we're treating
    // it as the trailing edge, or we've hit the `maxWait` limit.
    return (lastCallTime === undefined || (timeSinceLastCall >= wait) ||
      (timeSinceLastCall < 0) || (maxing && timeSinceLastInvoke >= maxWait));
  }

  function timerExpired() {
    var time = now();
    if (shouldInvoke(time)) {
      return trailingEdge(time);
    }
    // Restart the timer.
    timerId = setTimeout(timerExpired, remainingWait(time));
  }

  function trailingEdge(time) {
    timerId = undefined;

    // Only invoke if we have `lastArgs` which means `func` has been
    // debounced at least once.
    if (trailing && lastArgs) {
      return invokeFunc(time);
    }
    lastArgs = lastThis = undefined;
    return result;
  }

  function cancel() {
    if (timerId !== undefined) {
      clearTimeout(timerId);
    }
    lastInvokeTime = 0;
    lastArgs = lastCallTime = lastThis = timerId = undefined;
  }

  function flush() {
    return timerId === undefined ? result : trailingEdge(now());
  }

  function debounced() {
    var time = now(),
        isInvoking = shouldInvoke(time);

    lastArgs = arguments;
    lastThis = this;
    lastCallTime = time;

    if (isInvoking) {
      if (timerId === undefined) {
        return leadingEdge(lastCallTime);
      }
      if (maxing) {
        // Handle invocations in a tight loop.
        timerId = setTimeout(timerExpired, wait);
        return invokeFunc(lastCallTime);
      }
    }
    if (timerId === undefined) {
      timerId = setTimeout(timerExpired, wait);
    }
    return result;
  }
  debounced.cancel = cancel;
  debounced.flush = flush;
  return debounced;
}

module.exports = debounce;


/***/ }),

/***/ "b1ea":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Cell_vue_vue_type_template_id_3183297c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("91ea");
/* harmony import */ var _Cell_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("5a83");
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _Cell_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _Cell_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var _Cell_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("26fd");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("2877");








function injectStyles (context) {
  
  this["$style"] = (_Cell_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_2__["default"].locals || _Cell_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_2__["default"])

}

/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"])(
  _Cell_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Cell_vue_vue_type_template_id_3183297c___WEBPACK_IMPORTED_MODULE_0__[/* render */ "a"],
  _Cell_vue_vue_type_template_id_3183297c___WEBPACK_IMPORTED_MODULE_0__[/* staticRenderFns */ "b"],
  false,
  injectStyles,
  null,
  null
  
)

component.options.__file = "Cell.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "b4b0":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("1a8c"),
    isSymbol = __webpack_require__("ffd6");

/** Used as references for various `Number` constants. */
var NAN = 0 / 0;

/** Used to match leading and trailing whitespace. */
var reTrim = /^\s+|\s+$/g;

/** Used to detect bad signed hexadecimal string values. */
var reIsBadHex = /^[-+]0x[0-9a-f]+$/i;

/** Used to detect binary string values. */
var reIsBinary = /^0b[01]+$/i;

/** Used to detect octal string values. */
var reIsOctal = /^0o[0-7]+$/i;

/** Built-in method references without a dependency on `root`. */
var freeParseInt = parseInt;

/**
 * Converts `value` to a number.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to process.
 * @returns {number} Returns the number.
 * @example
 *
 * _.toNumber(3.2);
 * // => 3.2
 *
 * _.toNumber(Number.MIN_VALUE);
 * // => 5e-324
 *
 * _.toNumber(Infinity);
 * // => Infinity
 *
 * _.toNumber('3.2');
 * // => 3.2
 */
function toNumber(value) {
  if (typeof value == 'number') {
    return value;
  }
  if (isSymbol(value)) {
    return NAN;
  }
  if (isObject(value)) {
    var other = typeof value.valueOf == 'function' ? value.valueOf() : value;
    value = isObject(other) ? (other + '') : other;
  }
  if (typeof value != 'string') {
    return value === 0 ? value : +value;
  }
  value = value.replace(reTrim, '');
  var isBinary = reIsBinary.test(value);
  return (isBinary || reIsOctal.test(value))
    ? freeParseInt(value.slice(2), isBinary ? 2 : 8)
    : (reIsBadHex.test(value) ? NAN : +value);
}

module.exports = toNumber;


/***/ }),

/***/ "b770":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = module.exports = {};

/**
 * Loops through the collection and calls the callback for each element. if the callback returns truthy, the loop is broken and returns the same value.
 * @public
 * @param {*} collection The collection to loop through. Needs to have a length property set and have indices set from 0 to length - 1.
 * @param {function} callback The callback to be called for each element. The element will be given as a parameter to the callback. If this callback returns truthy, the loop is broken and the same value is returned.
 * @returns {*} The value that a callback has returned (if truthy). Otherwise nothing.
 */
utils.forEach = function(collection, callback) {
    for(var i = 0; i < collection.length; i++) {
        var result = callback(collection[i]);
        if(result) {
            return result;
        }
    }
};


/***/ }),

/***/ "c274":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("50bf");

module.exports = function batchProcessorMaker(options) {
    options             = options || {};
    var reporter        = options.reporter;
    var asyncProcess    = utils.getOption(options, "async", true);
    var autoProcess     = utils.getOption(options, "auto", true);

    if(autoProcess && !asyncProcess) {
        reporter && reporter.warn("Invalid options combination. auto=true and async=false is invalid. Setting async=true.");
        asyncProcess = true;
    }

    var batch = Batch();
    var asyncFrameHandler;
    var isProcessing = false;

    function addFunction(level, fn) {
        if(!isProcessing && autoProcess && asyncProcess && batch.size() === 0) {
            // Since this is async, it is guaranteed to be executed after that the fn is added to the batch.
            // This needs to be done before, since we're checking the size of the batch to be 0.
            processBatchAsync();
        }

        batch.add(level, fn);
    }

    function processBatch() {
        // Save the current batch, and create a new batch so that incoming functions are not added into the currently processing batch.
        // Continue processing until the top-level batch is empty (functions may be added to the new batch while processing, and so on).
        isProcessing = true;
        while (batch.size()) {
            var processingBatch = batch;
            batch = Batch();
            processingBatch.process();
        }
        isProcessing = false;
    }

    function forceProcessBatch(localAsyncProcess) {
        if (isProcessing) {
            return;
        }

        if(localAsyncProcess === undefined) {
            localAsyncProcess = asyncProcess;
        }

        if(asyncFrameHandler) {
            cancelFrame(asyncFrameHandler);
            asyncFrameHandler = null;
        }

        if(localAsyncProcess) {
            processBatchAsync();
        } else {
            processBatch();
        }
    }

    function processBatchAsync() {
        asyncFrameHandler = requestFrame(processBatch);
    }

    function clearBatch() {
        batch           = {};
        batchSize       = 0;
        topLevel        = 0;
        bottomLevel     = 0;
    }

    function cancelFrame(listener) {
        // var cancel = window.cancelAnimationFrame || window.mozCancelAnimationFrame || window.webkitCancelAnimationFrame || window.clearTimeout;
        var cancel = clearTimeout;
        return cancel(listener);
    }

    function requestFrame(callback) {
        // var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || function(fn) { return window.setTimeout(fn, 20); };
        var raf = function(fn) { return setTimeout(fn, 0); };
        return raf(callback);
    }

    return {
        add: addFunction,
        force: forceProcessBatch
    };
};

function Batch() {
    var batch       = {};
    var size        = 0;
    var topLevel    = 0;
    var bottomLevel = 0;

    function add(level, fn) {
        if(!fn) {
            fn = level;
            level = 0;
        }

        if(level > topLevel) {
            topLevel = level;
        } else if(level < bottomLevel) {
            bottomLevel = level;
        }

        if(!batch[level]) {
            batch[level] = [];
        }

        batch[level].push(fn);
        size++;
    }

    function process() {
        for(var level = bottomLevel; level <= topLevel; level++) {
            var fns = batch[level];

            for(var i = 0; i < fns.length; i++) {
                var fn = fns[i];
                fn();
            }
        }
    }

    function getSize() {
        return size;
    }

    return {
        add: add,
        process: process,
        size: getSize
    };
}


/***/ }),

/***/ "c413":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "\n.Cell_cell_2PZxO{position:relative;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center\n}\n.Cell_rotate-layer_1G3xT{cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none\n}\n.Cell_consumer_2bU1v,.Cell_provider_3A8pP,.Cell_rotate-layer_1G3xT{position:absolute;width:100%;height:100%\n}\n.Cell_provider_3A8pP{width:70%;height:50%\n}\n.Cell_consumer_2bU1v{width:50%;height:50%\n}\n.Cell_node_2MCVQ{position:absolute;width:101%;height:101%\n}\n.Cell_connections_OzCvZ{-webkit-transform-origin:center;transform-origin:center\n}", ""]);

// exports
exports.locals = {
	"cell": "Cell_cell_2PZxO",
	"rotate-layer": "Cell_rotate-layer_1G3xT",
	"consumer": "Cell_consumer_2bU1v",
	"provider": "Cell_provider_3A8pP",
	"node": "Cell_node_2MCVQ",
	"connections": "Cell_connections_OzCvZ"
};

/***/ }),

/***/ "c6ff":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("1df4");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("5bd6fb04", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "c8ba":
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1, eval)("this");
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ "c946":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Resize detection strategy that injects divs to elements in order to detect resize events on scroll events.
 * Heavily inspired by: https://github.com/marcj/css-element-queries/blob/master/src/ResizeSensor.js
 */



var forEach = __webpack_require__("b770").forEach;

module.exports = function(options) {
    options             = options || {};
    var reporter        = options.reporter;
    var batchProcessor  = options.batchProcessor;
    var getState        = options.stateHandler.getState;
    var hasState        = options.stateHandler.hasState;
    var idHandler       = options.idHandler;

    if (!batchProcessor) {
        throw new Error("Missing required dependency: batchProcessor");
    }

    if (!reporter) {
        throw new Error("Missing required dependency: reporter.");
    }

    //TODO: Could this perhaps be done at installation time?
    var scrollbarSizes = getScrollbarSizes();

    // Inject the scrollbar styling that prevents them from appearing sometimes in Chrome.
    // The injected container needs to have a class, so that it may be styled with CSS (pseudo elements).
    var styleId = "erd_scroll_detection_scrollbar_style";
    var detectionContainerClass = "erd_scroll_detection_container";
    injectScrollStyle(styleId, detectionContainerClass);

    function getScrollbarSizes() {
        var width = 500;
        var height = 500;

        var child = document.createElement("div");
        child.style.cssText = "position: absolute; width: " + width*2 + "px; height: " + height*2 + "px; visibility: hidden; margin: 0; padding: 0;";

        var container = document.createElement("div");
        container.style.cssText = "position: absolute; width: " + width + "px; height: " + height + "px; overflow: scroll; visibility: none; top: " + -width*3 + "px; left: " + -height*3 + "px; visibility: hidden; margin: 0; padding: 0;";

        container.appendChild(child);

        document.body.insertBefore(container, document.body.firstChild);

        var widthSize = width - container.clientWidth;
        var heightSize = height - container.clientHeight;

        document.body.removeChild(container);

        return {
            width: widthSize,
            height: heightSize
        };
    }

    function injectScrollStyle(styleId, containerClass) {
        function injectStyle(style, method) {
            method = method || function (element) {
                document.head.appendChild(element);
            };

            var styleElement = document.createElement("style");
            styleElement.innerHTML = style;
            styleElement.id = styleId;
            method(styleElement);
            return styleElement;
        }

        if (!document.getElementById(styleId)) {
            var containerAnimationClass = containerClass + "_animation";
            var containerAnimationActiveClass = containerClass + "_animation_active";
            var style = "/* Created by the element-resize-detector library. */\n";
            style += "." + containerClass + " > div::-webkit-scrollbar { display: none; }\n\n";
            style += "." + containerAnimationActiveClass + " { -webkit-animation-duration: 0.1s; animation-duration: 0.1s; -webkit-animation-name: " + containerAnimationClass + "; animation-name: " + containerAnimationClass + "; }\n";
            style += "@-webkit-keyframes " + containerAnimationClass +  " { 0% { opacity: 1; } 50% { opacity: 0; } 100% { opacity: 1; } }\n";
            style += "@keyframes " + containerAnimationClass +          " { 0% { opacity: 1; } 50% { opacity: 0; } 100% { opacity: 1; } }";
            injectStyle(style);
        }
    }

    function addAnimationClass(element) {
        element.className += " " + detectionContainerClass + "_animation_active";
    }

    function addEvent(el, name, cb) {
        if (el.addEventListener) {
            el.addEventListener(name, cb);
        } else if(el.attachEvent) {
            el.attachEvent("on" + name, cb);
        } else {
            return reporter.error("[scroll] Don't know how to add event listeners.");
        }
    }

    function removeEvent(el, name, cb) {
        if (el.removeEventListener) {
            el.removeEventListener(name, cb);
        } else if(el.detachEvent) {
            el.detachEvent("on" + name, cb);
        } else {
            return reporter.error("[scroll] Don't know how to remove event listeners.");
        }
    }

    function getExpandElement(element) {
        return getState(element).container.childNodes[0].childNodes[0].childNodes[0];
    }

    function getShrinkElement(element) {
        return getState(element).container.childNodes[0].childNodes[0].childNodes[1];
    }

    /**
     * Adds a resize event listener to the element.
     * @public
     * @param {element} element The element that should have the listener added.
     * @param {function} listener The listener callback to be called for each resize event of the element. The element will be given as a parameter to the listener callback.
     */
    function addListener(element, listener) {
        var listeners = getState(element).listeners;

        if (!listeners.push) {
            throw new Error("Cannot add listener to an element that is not detectable.");
        }

        getState(element).listeners.push(listener);
    }

    /**
     * Makes an element detectable and ready to be listened for resize events. Will call the callback when the element is ready to be listened for resize changes.
     * @private
     * @param {object} options Optional options object.
     * @param {element} element The element to make detectable
     * @param {function} callback The callback to be called when the element is ready to be listened for resize changes. Will be called with the element as first parameter.
     */
    function makeDetectable(options, element, callback) {
        if (!callback) {
            callback = element;
            element = options;
            options = null;
        }

        options = options || {};

        function debug() {
            if (options.debug) {
                var args = Array.prototype.slice.call(arguments);
                args.unshift(idHandler.get(element), "Scroll: ");
                if (reporter.log.apply) {
                    reporter.log.apply(null, args);
                } else {
                    for (var i = 0; i < args.length; i++) {
                        reporter.log(args[i]);
                    }
                }
            }
        }

        function isDetached(element) {
            function isInDocument(element) {
                return element === element.ownerDocument.body || element.ownerDocument.body.contains(element);
            }

            if (!isInDocument(element)) {
                return true;
            }

            // FireFox returns null style in hidden iframes. See https://github.com/wnr/element-resize-detector/issues/68 and https://bugzilla.mozilla.org/show_bug.cgi?id=795520
            if (window.getComputedStyle(element) === null) {
                return true;
            }

            return false;
        }

        function isUnrendered(element) {
            // Check the absolute positioned container since the top level container is display: inline.
            var container = getState(element).container.childNodes[0];
            var style = window.getComputedStyle(container);
            return !style.width || style.width.indexOf("px") === -1; //Can only compute pixel value when rendered.
        }

        function getStyle() {
            // Some browsers only force layouts when actually reading the style properties of the style object, so make sure that they are all read here,
            // so that the user of the function can be sure that it will perform the layout here, instead of later (important for batching).
            var elementStyle            = window.getComputedStyle(element);
            var style                   = {};
            style.position              = elementStyle.position;
            style.width                 = element.offsetWidth;
            style.height                = element.offsetHeight;
            style.top                   = elementStyle.top;
            style.right                 = elementStyle.right;
            style.bottom                = elementStyle.bottom;
            style.left                  = elementStyle.left;
            style.widthCSS              = elementStyle.width;
            style.heightCSS             = elementStyle.height;
            return style;
        }

        function storeStartSize() {
            var style = getStyle();
            getState(element).startSize = {
                width: style.width,
                height: style.height
            };
            debug("Element start size", getState(element).startSize);
        }

        function initListeners() {
            getState(element).listeners = [];
        }

        function storeStyle() {
            debug("storeStyle invoked.");
            if (!getState(element)) {
                debug("Aborting because element has been uninstalled");
                return;
            }

            var style = getStyle();
            getState(element).style = style;
        }

        function storeCurrentSize(element, width, height) {
            getState(element).lastWidth = width;
            getState(element).lastHeight  = height;
        }

        function getExpandChildElement(element) {
            return getExpandElement(element).childNodes[0];
        }

        function getWidthOffset() {
            return 2 * scrollbarSizes.width + 1;
        }

        function getHeightOffset() {
            return 2 * scrollbarSizes.height + 1;
        }

        function getExpandWidth(width) {
            return width + 10 + getWidthOffset();
        }

        function getExpandHeight(height) {
            return height + 10 + getHeightOffset();
        }

        function getShrinkWidth(width) {
            return width * 2 + getWidthOffset();
        }

        function getShrinkHeight(height) {
            return height * 2 + getHeightOffset();
        }

        function positionScrollbars(element, width, height) {
            var expand          = getExpandElement(element);
            var shrink          = getShrinkElement(element);
            var expandWidth     = getExpandWidth(width);
            var expandHeight    = getExpandHeight(height);
            var shrinkWidth     = getShrinkWidth(width);
            var shrinkHeight    = getShrinkHeight(height);
            expand.scrollLeft   = expandWidth;
            expand.scrollTop    = expandHeight;
            shrink.scrollLeft   = shrinkWidth;
            shrink.scrollTop    = shrinkHeight;
        }

        function injectContainerElement() {
            var container = getState(element).container;

            if (!container) {
                container                   = document.createElement("div");
                container.className         = detectionContainerClass;
                container.style.cssText     = "visibility: hidden; display: inline; width: 0px; height: 0px; z-index: -1; overflow: hidden; margin: 0; padding: 0;";
                getState(element).container = container;
                addAnimationClass(container);
                element.appendChild(container);

                var onAnimationStart = function () {
                    getState(element).onRendered && getState(element).onRendered();
                };

                addEvent(container, "animationstart", onAnimationStart);

                // Store the event handler here so that they may be removed when uninstall is called.
                // See uninstall function for an explanation why it is needed.
                getState(element).onAnimationStart = onAnimationStart;
            }

            return container;
        }

        function injectScrollElements() {
            function alterPositionStyles() {
                var style = getState(element).style;

                if(style.position === "static") {
                    element.style.position = "relative";

                    var removeRelativeStyles = function(reporter, element, style, property) {
                        function getNumericalValue(value) {
                            return value.replace(/[^-\d\.]/g, "");
                        }

                        var value = style[property];

                        if(value !== "auto" && getNumericalValue(value) !== "0") {
                            reporter.warn("An element that is positioned static has style." + property + "=" + value + " which is ignored due to the static positioning. The element will need to be positioned relative, so the style." + property + " will be set to 0. Element: ", element);
                            element.style[property] = 0;
                        }
                    };

                    //Check so that there are no accidental styles that will make the element styled differently now that is is relative.
                    //If there are any, set them to 0 (this should be okay with the user since the style properties did nothing before [since the element was positioned static] anyway).
                    removeRelativeStyles(reporter, element, style, "top");
                    removeRelativeStyles(reporter, element, style, "right");
                    removeRelativeStyles(reporter, element, style, "bottom");
                    removeRelativeStyles(reporter, element, style, "left");
                }
            }

            function getLeftTopBottomRightCssText(left, top, bottom, right) {
                left = (!left ? "0" : (left + "px"));
                top = (!top ? "0" : (top + "px"));
                bottom = (!bottom ? "0" : (bottom + "px"));
                right = (!right ? "0" : (right + "px"));

                return "left: " + left + "; top: " + top + "; right: " + right + "; bottom: " + bottom + ";";
            }

            debug("Injecting elements");

            if (!getState(element)) {
                debug("Aborting because element has been uninstalled");
                return;
            }

            alterPositionStyles();

            var rootContainer = getState(element).container;

            if (!rootContainer) {
                rootContainer = injectContainerElement();
            }

            // Due to this WebKit bug https://bugs.webkit.org/show_bug.cgi?id=80808 (currently fixed in Blink, but still present in WebKit browsers such as Safari),
            // we need to inject two containers, one that is width/height 100% and another that is left/top -1px so that the final container always is 1x1 pixels bigger than
            // the targeted element.
            // When the bug is resolved, "containerContainer" may be removed.

            // The outer container can occasionally be less wide than the targeted when inside inline elements element in WebKit (see https://bugs.webkit.org/show_bug.cgi?id=152980).
            // This should be no problem since the inner container either way makes sure the injected scroll elements are at least 1x1 px.

            var scrollbarWidth          = scrollbarSizes.width;
            var scrollbarHeight         = scrollbarSizes.height;
            var containerContainerStyle = "position: absolute; flex: none; overflow: hidden; z-index: -1; visibility: hidden; width: 100%; height: 100%; left: 0px; top: 0px;";
            var containerStyle          = "position: absolute; flex: none; overflow: hidden; z-index: -1; visibility: hidden; " + getLeftTopBottomRightCssText(-(1 + scrollbarWidth), -(1 + scrollbarHeight), -scrollbarHeight, -scrollbarWidth);
            var expandStyle             = "position: absolute; flex: none; overflow: scroll; z-index: -1; visibility: hidden; width: 100%; height: 100%;";
            var shrinkStyle             = "position: absolute; flex: none; overflow: scroll; z-index: -1; visibility: hidden; width: 100%; height: 100%;";
            var expandChildStyle        = "position: absolute; left: 0; top: 0;";
            var shrinkChildStyle        = "position: absolute; width: 200%; height: 200%;";

            var containerContainer      = document.createElement("div");
            var container               = document.createElement("div");
            var expand                  = document.createElement("div");
            var expandChild             = document.createElement("div");
            var shrink                  = document.createElement("div");
            var shrinkChild             = document.createElement("div");

            // Some browsers choke on the resize system being rtl, so force it to ltr. https://github.com/wnr/element-resize-detector/issues/56
            // However, dir should not be set on the top level container as it alters the dimensions of the target element in some browsers.
            containerContainer.dir              = "ltr";

            containerContainer.style.cssText    = containerContainerStyle;
            containerContainer.className        = detectionContainerClass;
            container.className                 = detectionContainerClass;
            container.style.cssText             = containerStyle;
            expand.style.cssText                = expandStyle;
            expandChild.style.cssText           = expandChildStyle;
            shrink.style.cssText                = shrinkStyle;
            shrinkChild.style.cssText           = shrinkChildStyle;

            expand.appendChild(expandChild);
            shrink.appendChild(shrinkChild);
            container.appendChild(expand);
            container.appendChild(shrink);
            containerContainer.appendChild(container);
            rootContainer.appendChild(containerContainer);

            function onExpandScroll() {
                getState(element).onExpand && getState(element).onExpand();
            }

            function onShrinkScroll() {
                getState(element).onShrink && getState(element).onShrink();
            }

            addEvent(expand, "scroll", onExpandScroll);
            addEvent(shrink, "scroll", onShrinkScroll);

            // Store the event handlers here so that they may be removed when uninstall is called.
            // See uninstall function for an explanation why it is needed.
            getState(element).onExpandScroll = onExpandScroll;
            getState(element).onShrinkScroll = onShrinkScroll;
        }

        function registerListenersAndPositionElements() {
            function updateChildSizes(element, width, height) {
                var expandChild             = getExpandChildElement(element);
                var expandWidth             = getExpandWidth(width);
                var expandHeight            = getExpandHeight(height);
                expandChild.style.width     = expandWidth + "px";
                expandChild.style.height    = expandHeight + "px";
            }

            function updateDetectorElements(done) {
                var width           = element.offsetWidth;
                var height          = element.offsetHeight;

                debug("Storing current size", width, height);

                // Store the size of the element sync here, so that multiple scroll events may be ignored in the event listeners.
                // Otherwise the if-check in handleScroll is useless.
                storeCurrentSize(element, width, height);

                // Since we delay the processing of the batch, there is a risk that uninstall has been called before the batch gets to execute.
                // Since there is no way to cancel the fn executions, we need to add an uninstall guard to all fns of the batch.

                batchProcessor.add(0, function performUpdateChildSizes() {
                    if (!getState(element)) {
                        debug("Aborting because element has been uninstalled");
                        return;
                    }

                    if (!areElementsInjected()) {
                        debug("Aborting because element container has not been initialized");
                        return;
                    }

                    if (options.debug) {
                        var w = element.offsetWidth;
                        var h = element.offsetHeight;

                        if (w !== width || h !== height) {
                            reporter.warn(idHandler.get(element), "Scroll: Size changed before updating detector elements.");
                        }
                    }

                    updateChildSizes(element, width, height);
                });

                batchProcessor.add(1, function updateScrollbars() {
                    if (!getState(element)) {
                        debug("Aborting because element has been uninstalled");
                        return;
                    }

                    if (!areElementsInjected()) {
                        debug("Aborting because element container has not been initialized");
                        return;
                    }

                    positionScrollbars(element, width, height);
                });

                if (done) {
                    batchProcessor.add(2, function () {
                        if (!getState(element)) {
                            debug("Aborting because element has been uninstalled");
                            return;
                        }

                        if (!areElementsInjected()) {
                          debug("Aborting because element container has not been initialized");
                          return;
                        }

                        done();
                    });
                }
            }

            function areElementsInjected() {
                return !!getState(element).container;
            }

            function notifyListenersIfNeeded() {
                function isFirstNotify() {
                    return getState(element).lastNotifiedWidth === undefined;
                }

                debug("notifyListenersIfNeeded invoked");

                var state = getState(element);

                // Don't notify the if the current size is the start size, and this is the first notification.
                if (isFirstNotify() && state.lastWidth === state.startSize.width && state.lastHeight === state.startSize.height) {
                    return debug("Not notifying: Size is the same as the start size, and there has been no notification yet.");
                }

                // Don't notify if the size already has been notified.
                if (state.lastWidth === state.lastNotifiedWidth && state.lastHeight === state.lastNotifiedHeight) {
                    return debug("Not notifying: Size already notified");
                }


                debug("Current size not notified, notifying...");
                state.lastNotifiedWidth = state.lastWidth;
                state.lastNotifiedHeight = state.lastHeight;
                forEach(getState(element).listeners, function (listener) {
                    listener(element);
                });
            }

            function handleRender() {
                debug("startanimation triggered.");

                if (isUnrendered(element)) {
                    debug("Ignoring since element is still unrendered...");
                    return;
                }

                debug("Element rendered.");
                var expand = getExpandElement(element);
                var shrink = getShrinkElement(element);
                if (expand.scrollLeft === 0 || expand.scrollTop === 0 || shrink.scrollLeft === 0 || shrink.scrollTop === 0) {
                    debug("Scrollbars out of sync. Updating detector elements...");
                    updateDetectorElements(notifyListenersIfNeeded);
                }
            }

            function handleScroll() {
                debug("Scroll detected.");

                if (isUnrendered(element)) {
                    // Element is still unrendered. Skip this scroll event.
                    debug("Scroll event fired while unrendered. Ignoring...");
                    return;
                }

                var width = element.offsetWidth;
                var height = element.offsetHeight;

                if (width !== getState(element).lastWidth || height !== getState(element).lastHeight) {
                    debug("Element size changed.");
                    updateDetectorElements(notifyListenersIfNeeded);
                } else {
                    debug("Element size has not changed (" + width + "x" + height + ").");
                }
            }

            debug("registerListenersAndPositionElements invoked.");

            if (!getState(element)) {
                debug("Aborting because element has been uninstalled");
                return;
            }

            getState(element).onRendered = handleRender;
            getState(element).onExpand = handleScroll;
            getState(element).onShrink = handleScroll;

            var style = getState(element).style;
            updateChildSizes(element, style.width, style.height);
        }

        function finalizeDomMutation() {
            debug("finalizeDomMutation invoked.");

            if (!getState(element)) {
                debug("Aborting because element has been uninstalled");
                return;
            }

            var style = getState(element).style;
            storeCurrentSize(element, style.width, style.height);
            positionScrollbars(element, style.width, style.height);
        }

        function ready() {
            callback(element);
        }

        function install() {
            debug("Installing...");
            initListeners();
            storeStartSize();

            batchProcessor.add(0, storeStyle);
            batchProcessor.add(1, injectScrollElements);
            batchProcessor.add(2, registerListenersAndPositionElements);
            batchProcessor.add(3, finalizeDomMutation);
            batchProcessor.add(4, ready);
        }

        debug("Making detectable...");

        if (isDetached(element)) {
            debug("Element is detached");

            injectContainerElement();

            debug("Waiting until element is attached...");

            getState(element).onRendered = function () {
                debug("Element is now attached");
                install();
            };
        } else {
            install();
        }
    }

    function uninstall(element) {
        var state = getState(element);

        if (!state) {
            // Uninstall has been called on a non-erd element.
            return;
        }

        // Uninstall may have been called in the following scenarios:
        // (1) Right between the sync code and async batch (here state.busy = true, but nothing have been registered or injected).
        // (2) In the ready callback of the last level of the batch by another element (here, state.busy = true, but all the stuff has been injected).
        // (3) After the installation process (here, state.busy = false and all the stuff has been injected).
        // So to be on the safe side, let's check for each thing before removing.

        // We need to remove the event listeners, because otherwise the event might fire on an uninstall element which results in an error when trying to get the state of the element.
        state.onExpandScroll && removeEvent(getExpandElement(element), "scroll", state.onExpandScroll);
        state.onShrinkScroll && removeEvent(getShrinkElement(element), "scroll", state.onShrinkScroll);
        state.onAnimationStart && removeEvent(state.container, "animationstart", state.onAnimationStart);

        state.container && element.removeChild(state.container);
    }

    return {
        makeDetectable: makeDetectable,
        addListener: addListener,
        uninstall: uninstall
    };
};


/***/ }),

/***/ "d6eb":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var prop = "_erd";

function initState(element) {
    element[prop] = {};
    return getState(element);
}

function getState(element) {
    return element[prop];
}

function cleanState(element) {
    delete element[prop];
}

module.exports = {
    initState: initState,
    getState: getState,
    cleanState: cleanState
};


/***/ }),

/***/ "eec4":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var forEach                 = __webpack_require__("b770").forEach;
var elementUtilsMaker       = __webpack_require__("5be5");
var listenerHandlerMaker    = __webpack_require__("49ad");
var idGeneratorMaker        = __webpack_require__("2cef");
var idHandlerMaker          = __webpack_require__("5058");
var reporterMaker           = __webpack_require__("abb4");
var browserDetector         = __webpack_require__("18e9");
var batchProcessorMaker     = __webpack_require__("c274");
var stateHandler            = __webpack_require__("d6eb");

//Detection strategies.
var objectStrategyMaker     = __webpack_require__("18d2");
var scrollStrategyMaker     = __webpack_require__("c946");

function isCollection(obj) {
    return Array.isArray(obj) || obj.length !== undefined;
}

function toArray(collection) {
    if (!Array.isArray(collection)) {
        var array = [];
        forEach(collection, function (obj) {
            array.push(obj);
        });
        return array;
    } else {
        return collection;
    }
}

function isElement(obj) {
    return obj && obj.nodeType === 1;
}

/**
 * @typedef idHandler
 * @type {object}
 * @property {function} get Gets the resize detector id of the element.
 * @property {function} set Generate and sets the resize detector id of the element.
 */

/**
 * @typedef Options
 * @type {object}
 * @property {boolean} callOnAdd    Determines if listeners should be called when they are getting added.
                                    Default is true. If true, the listener is guaranteed to be called when it has been added.
                                    If false, the listener will not be guarenteed to be called when it has been added (does not prevent it from being called).
 * @property {idHandler} idHandler  A custom id handler that is responsible for generating, setting and retrieving id's for elements.
                                    If not provided, a default id handler will be used.
 * @property {reporter} reporter    A custom reporter that handles reporting logs, warnings and errors.
                                    If not provided, a default id handler will be used.
                                    If set to false, then nothing will be reported.
 * @property {boolean} debug        If set to true, the the system will report debug messages as default for the listenTo method.
 */

/**
 * Creates an element resize detector instance.
 * @public
 * @param {Options?} options Optional global options object that will decide how this instance will work.
 */
module.exports = function(options) {
    options = options || {};

    //idHandler is currently not an option to the listenTo function, so it should not be added to globalOptions.
    var idHandler;

    if (options.idHandler) {
        // To maintain compatability with idHandler.get(element, readonly), make sure to wrap the given idHandler
        // so that readonly flag always is true when it's used here. This may be removed next major version bump.
        idHandler = {
            get: function (element) { return options.idHandler.get(element, true); },
            set: options.idHandler.set
        };
    } else {
        var idGenerator = idGeneratorMaker();
        var defaultIdHandler = idHandlerMaker({
            idGenerator: idGenerator,
            stateHandler: stateHandler
        });
        idHandler = defaultIdHandler;
    }

    //reporter is currently not an option to the listenTo function, so it should not be added to globalOptions.
    var reporter = options.reporter;

    if(!reporter) {
        //If options.reporter is false, then the reporter should be quiet.
        var quiet = reporter === false;
        reporter = reporterMaker(quiet);
    }

    //batchProcessor is currently not an option to the listenTo function, so it should not be added to globalOptions.
    var batchProcessor = getOption(options, "batchProcessor", batchProcessorMaker({ reporter: reporter }));

    //Options to be used as default for the listenTo function.
    var globalOptions = {};
    globalOptions.callOnAdd     = !!getOption(options, "callOnAdd", true);
    globalOptions.debug         = !!getOption(options, "debug", false);

    var eventListenerHandler    = listenerHandlerMaker(idHandler);
    var elementUtils            = elementUtilsMaker({
        stateHandler: stateHandler
    });

    //The detection strategy to be used.
    var detectionStrategy;
    var desiredStrategy = getOption(options, "strategy", "object");
    var strategyOptions = {
        reporter: reporter,
        batchProcessor: batchProcessor,
        stateHandler: stateHandler,
        idHandler: idHandler
    };

    if(desiredStrategy === "scroll") {
        if (browserDetector.isLegacyOpera()) {
            reporter.warn("Scroll strategy is not supported on legacy Opera. Changing to object strategy.");
            desiredStrategy = "object";
        } else if (browserDetector.isIE(9)) {
            reporter.warn("Scroll strategy is not supported on IE9. Changing to object strategy.");
            desiredStrategy = "object";
        }
    }

    if(desiredStrategy === "scroll") {
        detectionStrategy = scrollStrategyMaker(strategyOptions);
    } else if(desiredStrategy === "object") {
        detectionStrategy = objectStrategyMaker(strategyOptions);
    } else {
        throw new Error("Invalid strategy name: " + desiredStrategy);
    }

    //Calls can be made to listenTo with elements that are still being installed.
    //Also, same elements can occur in the elements list in the listenTo function.
    //With this map, the ready callbacks can be synchronized between the calls
    //so that the ready callback can always be called when an element is ready - even if
    //it wasn't installed from the function itself.
    var onReadyCallbacks = {};

    /**
     * Makes the given elements resize-detectable and starts listening to resize events on the elements. Calls the event callback for each event for each element.
     * @public
     * @param {Options?} options Optional options object. These options will override the global options. Some options may not be overriden, such as idHandler.
     * @param {element[]|element} elements The given array of elements to detect resize events of. Single element is also valid.
     * @param {function} listener The callback to be executed for each resize event for each element.
     */
    function listenTo(options, elements, listener) {
        function onResizeCallback(element) {
            var listeners = eventListenerHandler.get(element);
            forEach(listeners, function callListenerProxy(listener) {
                listener(element);
            });
        }

        function addListener(callOnAdd, element, listener) {
            eventListenerHandler.add(element, listener);

            if(callOnAdd) {
                listener(element);
            }
        }

        //Options object may be omitted.
        if(!listener) {
            listener = elements;
            elements = options;
            options = {};
        }

        if(!elements) {
            throw new Error("At least one element required.");
        }

        if(!listener) {
            throw new Error("Listener required.");
        }

        if (isElement(elements)) {
            // A single element has been passed in.
            elements = [elements];
        } else if (isCollection(elements)) {
            // Convert collection to array for plugins.
            // TODO: May want to check so that all the elements in the collection are valid elements.
            elements = toArray(elements);
        } else {
            return reporter.error("Invalid arguments. Must be a DOM element or a collection of DOM elements.");
        }

        var elementsReady = 0;

        var callOnAdd = getOption(options, "callOnAdd", globalOptions.callOnAdd);
        var onReadyCallback = getOption(options, "onReady", function noop() {});
        var debug = getOption(options, "debug", globalOptions.debug);

        forEach(elements, function attachListenerToElement(element) {
            if (!stateHandler.getState(element)) {
                stateHandler.initState(element);
                idHandler.set(element);
            }

            var id = idHandler.get(element);

            debug && reporter.log("Attaching listener to element", id, element);

            if(!elementUtils.isDetectable(element)) {
                debug && reporter.log(id, "Not detectable.");
                if(elementUtils.isBusy(element)) {
                    debug && reporter.log(id, "System busy making it detectable");

                    //The element is being prepared to be detectable. Do not make it detectable.
                    //Just add the listener, because the element will soon be detectable.
                    addListener(callOnAdd, element, listener);
                    onReadyCallbacks[id] = onReadyCallbacks[id] || [];
                    onReadyCallbacks[id].push(function onReady() {
                        elementsReady++;

                        if(elementsReady === elements.length) {
                            onReadyCallback();
                        }
                    });
                    return;
                }

                debug && reporter.log(id, "Making detectable...");
                //The element is not prepared to be detectable, so do prepare it and add a listener to it.
                elementUtils.markBusy(element, true);
                return detectionStrategy.makeDetectable({ debug: debug }, element, function onElementDetectable(element) {
                    debug && reporter.log(id, "onElementDetectable");

                    if (stateHandler.getState(element)) {
                        elementUtils.markAsDetectable(element);
                        elementUtils.markBusy(element, false);
                        detectionStrategy.addListener(element, onResizeCallback);
                        addListener(callOnAdd, element, listener);

                        // Since the element size might have changed since the call to "listenTo", we need to check for this change,
                        // so that a resize event may be emitted.
                        // Having the startSize object is optional (since it does not make sense in some cases such as unrendered elements), so check for its existance before.
                        // Also, check the state existance before since the element may have been uninstalled in the installation process.
                        var state = stateHandler.getState(element);
                        if (state && state.startSize) {
                            var width = element.offsetWidth;
                            var height = element.offsetHeight;
                            if (state.startSize.width !== width || state.startSize.height !== height) {
                                onResizeCallback(element);
                            }
                        }

                        if(onReadyCallbacks[id]) {
                            forEach(onReadyCallbacks[id], function(callback) {
                                callback();
                            });
                        }
                    } else {
                        // The element has been unisntalled before being detectable.
                        debug && reporter.log(id, "Element uninstalled before being detectable.");
                    }

                    delete onReadyCallbacks[id];

                    elementsReady++;
                    if(elementsReady === elements.length) {
                        onReadyCallback();
                    }
                });
            }

            debug && reporter.log(id, "Already detecable, adding listener.");

            //The element has been prepared to be detectable and is ready to be listened to.
            addListener(callOnAdd, element, listener);
            elementsReady++;
        });

        if(elementsReady === elements.length) {
            onReadyCallback();
        }
    }

    function uninstall(elements) {
        if(!elements) {
            return reporter.error("At least one element is required.");
        }

        if (isElement(elements)) {
            // A single element has been passed in.
            elements = [elements];
        } else if (isCollection(elements)) {
            // Convert collection to array for plugins.
            // TODO: May want to check so that all the elements in the collection are valid elements.
            elements = toArray(elements);
        } else {
            return reporter.error("Invalid arguments. Must be a DOM element or a collection of DOM elements.");
        }

        forEach(elements, function (element) {
            eventListenerHandler.removeAllListeners(element);
            detectionStrategy.uninstall(element);
            stateHandler.cleanState(element);
        });
    }

    return {
        listenTo: listenTo,
        removeListener: eventListenerHandler.removeListener,
        removeAllListeners: eventListenerHandler.removeAllListeners,
        uninstall: uninstall
    };
};

function getOption(options, name, defaultValue) {
    var value = options[name];

    if((value === undefined || value === null) && defaultValue !== undefined) {
        return defaultValue;
    }

    return value;
}


/***/ }),

/***/ "fb15":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _setPublicPath__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("1eb2");
/* harmony import */ var _entry__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("4a74");
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _entry__WEBPACK_IMPORTED_MODULE_1__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _entry__WEBPACK_IMPORTED_MODULE_1__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/* harmony default export */ __webpack_exports__["default"] = (_entry__WEBPACK_IMPORTED_MODULE_1__["default"]);



/***/ }),

/***/ "febb":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PlayingField_vue_vue_type_template_id_dd8085ea___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("81f8");
/* harmony import */ var _PlayingField_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("8943");
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _PlayingField_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _PlayingField_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var _PlayingField_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("1c73");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("2877");








function injectStyles (context) {
  
  this["$style"] = (_PlayingField_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_2__["default"].locals || _PlayingField_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_2__["default"])

}

/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"])(
  _PlayingField_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PlayingField_vue_vue_type_template_id_dd8085ea___WEBPACK_IMPORTED_MODULE_0__[/* render */ "a"],
  _PlayingField_vue_vue_type_template_id_dd8085ea___WEBPACK_IMPORTED_MODULE_0__[/* staticRenderFns */ "b"],
  false,
  injectStyles,
  null,
  null
  
)

component.options.__file = "PlayingField.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "ffd6":
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__("3729"),
    isObjectLike = __webpack_require__("1310");

/** `Object#toString` result references. */
var symbolTag = '[object Symbol]';

/**
 * Checks if `value` is classified as a `Symbol` primitive or object.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
 * @example
 *
 * _.isSymbol(Symbol.iterator);
 * // => true
 *
 * _.isSymbol('abc');
 * // => false
 */
function isSymbol(value) {
  return typeof value == 'symbol' ||
    (isObjectLike(value) && baseGetTag(value) == symbolTag);
}

module.exports = isSymbol;


/***/ })

/******/ })["default"];
//# sourceMappingURL=netwalk-vue.common.js.map